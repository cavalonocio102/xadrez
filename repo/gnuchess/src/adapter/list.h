/**
 * list.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


/**
 * Dolar:
 *     ~> Vai morar nós estados unidos então !
 *     ~> O Governo consegue ler meus pensamentos...
 */
#ifndef LIST_H
#define LIST_H

    #include "board.h"
    #include "move.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int ListSize = 256;

        /**
         * Tipos.
         */

        /**
         *
         */
        struct list_t
        {
            /**
             *
             */
            sint16 size;

            /**
             *
             */
            move_t move[ListSize];

            /**
             *
             */
            sint16 value[ListSize];
        };

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool list_is_ok(const list_t * list);

        /**
         *
         */
        extern void list_clear(list_t * list);

        /**
         *
         */
        extern void list_add(list_t * list, int move, int value = 0);

        /**
         *
         */
        extern void list_remove(list_t * list, int index);

        /**
         *
         */
        extern bool list_is_empty(const list_t * list);

        /**
         *
         */
        extern int  list_size(const list_t * list);

        /**
         *
         */
        extern int  list_move(const list_t * list, int index);

        /**
         *
         */
        extern int  list_value(const list_t * list, int index);

        /**
         *
         */
        extern void list_copy(list_t * dst, const list_t * src);

        /**
         *
         */
        extern void list_note(list_t * list);

        /**
         *
         */
        extern void list_sort(list_t * list);

        /**
         *
         */
        extern bool list_contain(const list_t * list, int move);

        /**
         *
         */
        extern bool list_equal(list_t * list_1, list_t * list_2);

        /**
         *
         */
        extern void list_disp(const list_t * list, const board_t * board);
    }

#endif
