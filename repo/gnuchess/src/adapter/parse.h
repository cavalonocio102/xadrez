/**
 * parse.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef PARSE_H
#define PARSE_H

    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int STAR_NUMBER = 16;

        /**
         *
         */
        const int KEYWORD_NUMBER = 256;

        /**
         * Tipos.
         */

        /**
         *
         */
        struct parse_t
        {
            /**
             *
             */
            const char * string;

            /**
             *
             */
            int pos;

            /**
             *
             */
            int keyword_nb;

            /**
             *
             */
            const char * keyword[KEYWORD_NUMBER];
        };

        /**
         * Variáveis.
         */

        /**
         *
         */
        extern char * Star[STAR_NUMBER];

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool match(char string[], const char pattern[]);

        /**
         *
         */
        extern void parse_open(parse_t * parse, const char string[]);

        /**
         *
         */
        extern void parse_close(parse_t * parse);

        /**
         *
         */
        extern void parse_add_keyword(parse_t * parse, const char keyword[]);

        /**
         *
         */
        extern bool parse_get_word(parse_t * parse, char string[], int size);

        /**
         *
         */
        extern bool parse_get_string(parse_t * parse, char string[], int size);
    }

#endif
