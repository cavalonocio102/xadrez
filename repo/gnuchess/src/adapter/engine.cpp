/**
 * engine.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include <cerrno>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <sys/types.h>
#include <unistd.h>

#include "engine.h"
#include "io.h"
#include "option.h"
#include "util.h"
#include "components.h"


/**
 *
 */
namespace adapter
{
    /**
     * Constantes.
     */

    /**
     *
     */
    static const bool UseNice = false;

    /**
     *
     */
    static const int StringSize = 4096;

    /**
     * Variáveis.
     */

    /**
     *
     */
    engine_t Engine[1];

    /**
     * Protótipos
     */

    /**
     * Funções.
     */

    /**
     *
     */
    bool engine_is_ok(const engine_t * engine)
    {
        if (engine == NULL)
        {
            return false;
        }

        if (!io_is_ok(engine->io))
        {
            return false;
        }

        return true;
    }

    /**
     *
     */
    void engine_open(engine_t * engine)
    {
        engine->io->in_fd = pipefd_e2a[0];
        engine->io->out_fd = pipefd_a2e[1];
        engine->io->name = "ENGINE";

        io_init(engine->io);
    }

    /**
     *
     */
    void engine_close(engine_t * engine)
    {
        ASSERT(engine_is_ok(engine));
        io_close(engine->io);
    }

    /**
     *
     */
    void engine_get(engine_t * engine, char string[], int size)
    {
        ASSERT(engine_is_ok(engine));
        ASSERT(string != NULL);
        ASSERT(size >= 256);

        while (!io_line_ready(engine->io))
        {
            io_get_update(engine->io);
        }

        if (!io_get_line(engine->io, string, size))
        {
            /**
             * EOF.
             */
            exit(EXIT_SUCCESS);
        }
    }

    /**
     *
     */
    void engine_send(engine_t * engine, const char format[], ...)
    {
        va_list arg_list;
        char string[StringSize];

        ASSERT(engine_is_ok(engine));
        ASSERT(format != NULL);

        /**
         * Formato.
         */

        va_start(arg_list, format);
        vsprintf(string, format, arg_list);
        va_end(arg_list);

        /**
         * Enviar.
         */
        io_send(engine->io, "%s", string);
    }

    /**
     *
     */
    void engine_send_queue(engine_t * engine, const char format[], ...)
    {
        va_list arg_list;
        char string[StringSize];

        ASSERT(engine_is_ok(engine));
        ASSERT(format != NULL);

        /**
         * Formato.
         */

        va_start(arg_list, format);
        vsprintf(string, format, arg_list);
        va_end(arg_list);

        /**
         * Enviar.
         */
        io_send_queue(engine->io, "%s", string);
    }
}
