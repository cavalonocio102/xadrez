/**
 * hash.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include "board.h"
#include "hash.h"
#include "piece.h"
#include "random.h"
#include "square.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Variáveis.
     */

    /**
     *
     */
    static uint64 Castle64[16];

    /**
     * Protótipos.
     */

    /**
     *
     */
    static uint64 hash_castle_key_debug (int flags);

    /**
     * Funções.
     */

    /**
     *
     */
    void hash_init()
    {
        int i;

        for (i = 0; i < 16; i++)
        {
            Castle64[i] = hash_castle_key_debug(i);
        }
    }

    /**
     *
     */
    uint64 hash_key(const board_t * board)
    {
        uint64 key;
        int colour;
        const uint8 * ptr;
        int sq, piece;

        ASSERT(board_is_ok(board));

        /**
         * Iniciar.
         */
        key = 0;

        /**
         * Peças.
         */
        for (colour = 1; colour <= 2; colour++)
        {
            for (ptr = board->list[colour]; (sq=*ptr) != SquareNone; ptr++)
            {
                piece = board->square[sq];
                key ^= hash_piece_key(piece, sq);
            }
        }

        /**
         * Bandeiras do castelo.
         */
        key ^= hash_castle_key(board_flags(board));

        /**
         * Praça de passagem.
         */
        sq = board->ep_square;

        if (sq != SquareNone)
        {
            key ^= hash_ep_key(sq);
        }

        /**
         * Alternar.
         */
        key ^= hash_turn_key(board->turn);

        return key;
    }

    /**
     *
     */
    uint64 hash_piece_key(int piece, int square)
    {
        ASSERT(piece_is_ok(piece));
        ASSERT(square_is_ok(square));

        return random_64(RandomPiece + piece_to_12(piece) * 64 + square_to_64(square));
    }

    /**
     *
     */
    uint64 hash_castle_key(int flags)
    {
        ASSERT((flags&~0xF) == 0);

        return Castle64[flags];
    }

    /**
     *
     */
    static uint64 hash_castle_key_debug(int flags)
    {
        uint64 key;
        int i;

        ASSERT((flags&~0xF) == 0);
        key = 0;

        for (i = 0; i < 4; i++)
        {
            if ((flags & (1 << i)) != 0)
            {
                key ^= random_64(RandomCastle + i);
            }
        }

        return key;
    }

    /**
     *
     */
    uint64 hash_ep_key(int square)
    {
        ASSERT(square_is_ok(square));

        return random_64(RandomEnPassant + square_file(square));
    }

    /**
     *
     */
    uint64 hash_turn_key(int colour)
    {
        ASSERT(colour_is_ok(colour));

        return (colour_is_white(colour)) ? random_64(RandomTurn) : 0;
    }
}
