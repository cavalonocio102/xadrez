/**
 * engine.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef ENGINE_H
#define ENGINE_H

    #include "io.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Tipos.
         */

        /**
         *
         */
        struct engine_t
        {
            io_t io[1];
        };

        /**
         * Variáveis.
         */

        /**
         *
         */
        extern engine_t Engine[1];

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool engine_is_ok(const engine_t * engine);

        /**
         *
         */
        extern void engine_open(engine_t * engine);

        /**
         *
         */
        extern void engine_close(engine_t * engine);

        /**
         *
         */
        extern void engine_get(engine_t * engine, char string[], int size);

        /**
         *
         */
        extern void engine_send(engine_t * engine, const char format[], ...);

        /**
         *
         */
        extern void engine_send_queue(engine_t * engine, const char format[], ...);
    }

#endif
