/**
 * line.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef LINE_H
#define LINE_H

    #include "board.h"
    #include "move.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int LineSize = 256;

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool line_is_ok(const move_t line[]);

        /**
         *
         */
        extern void line_clear(move_t line[]);

        /**
         *
         */
        extern void line_copy(move_t dst[], const move_t src[]);

        /**
         *
         */
        extern bool line_from_can(move_t line[], const board_t * board, const char string[], int size);

        /**
         *
         */
        extern bool line_to_can(const move_t line[], const board_t * board, char string[], int size);

        /**
         *
         */
        extern bool line_to_san(const move_t line[], const board_t * board, char string[], int size);
    }

#endif
