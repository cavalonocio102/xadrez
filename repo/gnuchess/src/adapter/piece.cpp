/**
 * piece.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include <cstring>

#include "colour.h"
#include "piece.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Constantes.
     */

    /**
     * -BW.
     */
    static const uint8 MakePawn[ColourNb] =
    {
        /**
         *
         */
        PieceNone256,

        /**
         *
         */
        BlackPawn256,

        /**
         *
         */
        WhitePawn256
    };

    /**
     *
     */
    static const uint8 PieceFrom12[12] =
    {
        /**
         *
         */
        BlackPawn256,

        /**
         *
         */
        WhitePawn256,

        /**
         *
         */
        BlackKnight256,

        /**
         *
         */
        WhiteKnight256,

        /**
         *
         */
        BlackBishop256,

        /**
         *
         */
        WhiteBishop256,

        /**
         *
         */
        BlackRook256,

        /**
         *
         */
        WhiteRook256,

        /**
         *
         */
        BlackQueen256,

        /**
         *
         */
        WhiteQueen256,

        /**
         *
         */
        BlackKing256,

        /**
         *
         */
        WhiteKing256,
    };

    /**
     *
     */
    static const char PieceString[12 + 1] = "pPnNbBrRqQkK";

    /**
     * Variáveis.
     */

    /**
     *
     */
    static sint8 PieceTo12[256];

    /**
     * Funções.
     */

    /**
     *
     */
    void piece_init()
    {
        int piece;

        for (piece = 0; piece < 256; piece++)
        {
            PieceTo12[piece] = -1;
        }

        for (piece = 0; piece < 12; piece++)
        {
            PieceTo12[PieceFrom12[piece]] = piece;
        }
    }

    /**
     *
     */
    bool piece_is_ok(int piece)
    {
        if (piece < 0 || piece >= 256)
        {
            return false;
        }

        if (PieceTo12[piece] < 0)
        {
            return false;
        }

        return true;
    }

    /**
     *
     */
    int piece_make_pawn(int colour)
    {
        ASSERT(colour_is_ok(colour));

        return MakePawn[colour];
    }

    /**
     *
     */
    int piece_pawn_opp(int piece)
    {
        ASSERT(piece == BlackPawn256 || piece == WhitePawn256);

        return piece ^ 15;
    }

    /**
     *
     */
    int piece_colour(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return piece & 3;
    }

    /**
     *
     */
    int piece_type(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return piece & ~3;
    }

    /**
     *
     */
    bool piece_is_pawn(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return (piece & PawnFlags) != 0;
    }

    /**
     *
     */
    bool piece_is_knight(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return (piece & KnightFlag) != 0;
    }

    /**
     *
     */
    bool piece_is_bishop(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return (piece & QueenFlags) == BishopFlag;
    }

    /**
     *
     */
    bool piece_is_rook(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return (piece & QueenFlags) == RookFlag;
    }

    /**
     *
     */
    bool piece_is_queen(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return (piece & QueenFlags) == QueenFlags;
    }

    /**
     *
     */
    bool piece_is_king(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return (piece & KingFlag) != 0;
    }

    /**
     *
     */
    bool piece_is_slider(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return (piece & QueenFlags) != 0;
    }

    /**
     *
     */
    int piece_to_12(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return PieceTo12[piece];
    }

    /**
     *
     */
    int piece_from_12(int piece)
    {
        ASSERT(piece >= 0 && piece < 12);

        return PieceFrom12[piece];
    }

    /**
     *
     */
    int piece_to_char(int piece)
    {
        ASSERT(piece_is_ok(piece));

        return PieceString[piece_to_12(piece)];
    }

    /**
     *
     */
    int piece_from_char(int c)
    {
        const char * ptr;
        ptr = strchr(PieceString, c);

        if (ptr == NULL)
        {
            return PieceNone256;
        }

        return piece_from_12(ptr - PieceString);
    }

    /**
     *
     */
    bool char_is_piece(int c)
    {
        return strchr("PNBRQK", c) != NULL;
    }
}
