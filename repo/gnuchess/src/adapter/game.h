/**
 * game.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef GAME_H
#define GAME_H

    #include "board.h"
    #include "move.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int GameSize = 4096;

        /**
         *
         */
        enum status_t
        {
            PLAYING,
            WHITE_MATES,
            BLACK_MATES,
            STALEMATE,
            DRAW_MATERIAL,
            DRAW_FIFTY,
            DRAW_REPETITION
        };

        /**
         * Tipos.
         */

        /**
         *
         */
        struct game_t
        {
            /**
             *
             */
            board_t start_board[1];

            /**
             *
             */
            board_t board[1];

            /**
             *
             */
            sint16 size;

            /**
             *
             */
            sint16 pos;

            /**
             *
             */
            sint8 status;

            /**
             *
             */
            move_t move[GameSize];

            /**
             *
             */
            uint64 key[GameSize];
        };

        /**
         * Variáveis.
         */

        /**
         *
         */
        extern game_t Game[1];

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool game_is_ok(const game_t * game);

        /**
         *
         */
        extern void game_clear(game_t * game);

        /**
         *
         */
        extern bool game_init(game_t * game, const char fen[]);

        /**
         *
         */
        extern int  game_status(const game_t * game);

        /**
         *
         */
        extern int  game_size(const game_t * game);

        /**
         *
         */
        extern int  game_pos(const game_t * game);

        /**
         *
         */
        extern int  game_move(const game_t * game, int pos);

        /**
         *
         */
        extern void game_get_board(const game_t * game, board_t * board, int pos = -1);

        /**
         *
         */
        extern int  game_turn(const game_t * game);

        /**
         *
         */
        extern int  game_move_nb(const game_t * game);

        /**
         *
         */
        extern void game_add_move(game_t * game, int move);

        /**
         *
         */
        extern void game_rem_move(game_t * game);

        /**
         *
         */
        extern void game_goto(game_t * game, int pos);

        /**
         *
         */
        extern void game_disp(const game_t * game);
    }

#endif
