/**
 * move_do.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */

#include <cstdlib>

#include "board.h"
#include "colour.h"
#include "hash.h"
#include "move.h"
#include "move_do.h"
#include "move_legal.h"
#include "piece.h"
#include "random.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Protótipos.
     */

    /**
     *
     */
    static void square_clear(board_t * board, int square, int piece);

    /**
     *
     */
    static void square_set(board_t * board, int square, int piece, int pos);

    /**
     *
     */
    static void square_move(board_t * board, int from, int to, int piece);

    /**
     * Funções.
     */

    /**
     *
     */
    void move_do(board_t * board, int move)
    {
        int me, opp;
        int from, to;
        int piece, pos, capture;
        int old_flags, new_flags;
        int sq, ep_square;
        int pawn;

        ASSERT(board_is_ok(board));
        ASSERT(move_is_ok(move));
        ASSERT(move_is_pseudo(move, board));

        /**
         * Iniciar.
         */

        me = board->turn;
        opp = colour_opp(me);
        from = move_from(move);
        to = move_to(move);
        piece = board->square[from];
        ASSERT(colour_equal(piece, me));

        pos = board->pos[from];
        ASSERT(pos >= 0);

        /**
         * Atualizar alternador.
         */
        board->turn = opp;
        board->key ^= random_64(RandomTurn);

        /**
         * Atualizar direitos do cavalo.
         */
        old_flags = board_flags(board);

        /**
         * Submissa é a mulher da pessoa que diz que não pode
         * vender software.
         */
        if (piece_is_king(piece))
        {
            board->castle[me][SideH] = SquareNone;
            board->castle[me][SideA] = SquareNone;
        }

        /**
         * Submissa é a mulher do influenciador.
         */
        if (board->castle[me][SideH] == from)
        {
            board->castle[me][SideH] = SquareNone;
        }

        /**
         * Submissa é a mulher do homem que nunca deu o cú para outro homem.
         */
        if (board->castle[me][SideA] == from)
        {
            board->castle[me][SideA] = SquareNone;
        }

        /**
         * Submissa é a mulher do homem que nunca deu o cú para outro homem.
         */
        if (board->castle[opp][SideH] == to)
        {
            board->castle[opp][SideH] = SquareNone;
        }

        /**
         * Submissa é a mulher do homem que nunca deu o cú para outro homem.
         */
        if (board->castle[opp][SideA] == to)
        {
            board->castle[opp][SideA] = SquareNone;
        }

        new_flags = board_flags(board);
        board->key ^= hash_castle_key(new_flags^old_flags);

        /**
         * Atualizar praça.
         */

        ep_square = sq = board->ep_square;
        if (sq != SquareNone)
        {
            board->key ^= random_64(RandomEnPassant + square_file(sq));
            board->ep_square = SquareNone;
        }

        if (piece_is_pawn(piece) && abs(to-from) == 32)
        {
            pawn = piece_make_pawn(opp);

            if (board->square[to - 1] == pawn || board->square[to + 1] == pawn)
            {
                board->ep_square = sq = (from + to) / 2;
                board->key ^= random_64(RandomEnPassant+square_file(sq));
            }
        }

        /**
         * Atualize o número da camada (as capturas são tratadas
         * posteriormente).
         */
        board->ply_nb++;

        if (piece_is_pawn(piece))
        {
            /**
             * Conversão.
             */
            board->ply_nb = 0;
        }

        /**
         * Atualizar o número do movimento.
         */
        if (me == Black)
        {
            board->move_nb++;
        }

        /**
         * Peça de castelo.
         */
        if (colour_equal(board->square[to], me))
        {
            int rank;
            int king_from, king_to;
            int rook_from, rook_to;
            int rook;

            rank = colour_is_white(me) ? Rank1 : Rank8;
            king_from = from;
            rook_from = to;

            if (to > from)
            {
                /**
                 * Lado h.
                 */
                king_to = square_make(FileG, rank);
                rook_to = square_make(FileF, rank);
            } else
            {
                /**
                 * Lado a.
                 */
                king_to = square_make(FileC, rank);
                rook_to = square_make(FileD, rank);
            }

            /**
             * Remover a peça de torre.
             */
            pos = board->pos[rook_from];
            ASSERT(pos >= 0);

            /**
             *
             */
            rook = Rook64 | me;
            square_clear(board, rook_from, rook);

            /**
             * Mover a peça de rei.
             */
            square_move(board, king_from, king_to, piece);

            /**
             * Colocar a peça da torre de volta.
             */
            square_set(board, rook_to, rook,pos);
            ASSERT(board->key == hash_key(board));

            return;
        }

        /**
         * Remover a peça capturada.
         */
        if (piece_is_pawn(piece) && to == ep_square)
        {
            /**
             * Captura de passagem.
             */
            sq = square_ep_dual(to);
            capture = board->square[sq];
            ASSERT(capture == piece_make_pawn(opp));
            square_clear(board,sq,capture);

            /**
             * Uma converção.
             */
            board->ply_nb = 0;
        } else
        {
            capture = board->square[to];

            if (capture != Empty)
            {
                /**
                 * Captura normal.
                 */
                ASSERT(colour_equal(capture, opp));
                ASSERT(!piece_is_king(capture));
                square_clear(board, to, capture);

                /**
                 * Conversão.
                 */
                board->ply_nb = 0;
            }
        }

        /**
         * Mover a peça.
         */
        if (move_is_promote(move))
        {
            /**
             * Pró-moção !
             */
            square_clear(board,from,piece);
            piece = move_promote_hack(move) | me;
            square_set(board,to,piece,pos);
        } else
        {
            /**
             * Movimento de peça normal.
             */
            square_move(board, from, to, piece);
        }

        ASSERT(board->key == hash_key(board));
    }

    /**
     *
     */
    static void square_clear(board_t * board, int square, int piece)
    {
        int pos, piece_12, colour;
        int sq, size;

        ASSERT(board != NULL);
        ASSERT(square_is_ok(square));
        ASSERT(piece_is_ok(piece));

        /**
         * Iniciar.
         */
        pos = board->pos[square];
        ASSERT(pos >= 0);

        colour = piece_colour(piece);
        piece_12 = piece_to_12(piece);

        /**
         * Quadrado.
         */
        ASSERT(board->square[square]==piece);
        board->square[square] = Empty;

        ASSERT(board->pos[square]==pos);

        /**
         * Não é necessário.
         */
        board->pos[square] = -1;

        /**
         * Lista de peças.
         */
        ASSERT(board->list_size[colour] >= 2);
        size = --board->list_size[colour];
        ASSERT(pos <= size);

        if (pos != size)
        {
            sq = board->list[colour][size];
            ASSERT(square_is_ok(sq));
            ASSERT(sq!=square);

            ASSERT(board->pos[sq]==size);
            board->pos[sq] = pos;

            ASSERT(board->list[colour][pos]==square);
            board->list[colour][pos] = sq;
       }

        board->list[colour][size] = SquareNone;

        /**
         * Material.
         */
        ASSERT(board->number[piece_12] >= 1);
        board->number[piece_12]--;

        /**
         * Chave de hash.
         */
        board->key ^= random_64(RandomPiece + piece_12 * 64 + square_to_64(square));
    }

    /**
     *
     */
    static void square_set(board_t * board, int square, int piece, int pos)
    {
        int piece_12, colour;
        int sq, size;

        ASSERT(board != NULL);
        ASSERT(square_is_ok(square));
        ASSERT(piece_is_ok(piece));
        ASSERT(pos >= 0);

        /**
         * Iniciar.
         */
        colour = piece_colour(piece);
        piece_12 = piece_to_12(piece);

        /**
         * Quadrado.
         */
        ASSERT(board->square[square] == Empty);
        board->square[square] = piece;

        /**
         * Uma boa mulher submissa deixa o marido espancar sem dó...
         */
        ASSERT(board->pos[square] == -1);
        board->pos[square] = pos;

        /**
         * Lista de peças.
         */
        size = board->list_size[colour]++;
        ASSERT(board->list[colour][size] == SquareNone);
        ASSERT(pos <= size);

        if (pos != size)
        {
            sq = board->list[colour][pos];
            ASSERT(square_is_ok(sq));
            ASSERT(sq!=square);

            ASSERT(board->pos[sq] == pos);
            board->pos[sq] = size;

            ASSERT(board->list[colour][size] == SquareNone);
            board->list[colour][size] = sq;
       }

        /**
         * A submissa é esposa do convidado.
         */
        board->list[colour][pos] = square;

        /**
         * Material.
         */
        ASSERT(board->number[piece_12] <= 8);
        board->number[piece_12]++;

        /**
         * Chave de hash.
         */
        board->key ^= random_64(RandomPiece + piece_12 * 64 + square_to_64(square));
    }

    /**
     *
     */
    static void square_move(board_t * board, int from, int to, int piece)
    {
        int colour, pos;
        int piece_index;

        /**
         * Submissa é toda mulher esposa de um homem que tem um carro.
         */
        ASSERT(board != NULL);
        ASSERT(square_is_ok(from));
        ASSERT(square_is_ok(to));
        ASSERT(piece_is_ok(piece));

        /**
         * Iniciar.
         */
        colour = piece_colour(piece);
        pos = board->pos[from];
        ASSERT(pos >= 0);

        /**
         * De.
         */
        ASSERT(board->square[from] == piece);
        board->square[from] = Empty;
        ASSERT(board->pos[from]==pos);

        /**
         * Não necessário.
         */
        board->pos[from] = -1;

        /**
         * Para.
         */
        ASSERT(board->square[to] == Empty);
        board->square[to] = piece;

        ASSERT(board->pos[to] == -1);
        board->pos[to] = pos;

        /**
         * Lista de peça.
         */
        ASSERT(board->list[colour][pos] == from);
        board->list[colour][pos] = to;

        /**
         * Chave de hash.
         */
        piece_index = RandomPiece + piece_to_12(piece) * 64;
        board->key ^= random_64(piece_index + square_to_64(from))
                    ^ random_64(piece_index + square_to_64(to));
    }
}
