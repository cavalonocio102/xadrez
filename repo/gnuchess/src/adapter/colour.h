/**
 * colour.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef COLOUR_H
#define COLOUR_H

    #include "util.h"

    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int BlackFlag = 1 << 0;

        /**
         *
         */
        const int WhiteFlag = 1 << 1;

        /**
         *
         */
        const int ColourNone = 0;

        /**
         *
         */
        const int Black = BlackFlag;

        /**
         *
         */
        const int White = WhiteFlag;

        /**
         *
         */
        const int ColourNb = 3;

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool colour_is_ok(int colour);

        /**
         *
         */
        extern bool colour_is_white(int colour);

        /**
         *
         */
        extern bool colour_is_black(int colour);

        /**
         *
         */
        extern bool colour_equal(int colour_1, int colour_2);

        /**
         *
         */
        extern int  colour_opp(int colour);
    }

#endif
