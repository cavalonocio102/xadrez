/**
 * option.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include <cstdlib>
#include <cstring>

#include "option.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Constantes.
     */

    /**
     *
     */
    static const bool UseDebug = false;

    /**
     * Tipos.
     */

    /**
     *
     */
    struct option_t
    {
        /**
         *
         */
        const char * var;

        /**
         *
         */
        const char * val;
    };

    /**
     * Variáveis.
     */

    /**
     *
     */
    static option_t Option[] =
    {
        {"OptionFile",        NULL,}, // Sequência de desenhos.

        /**
         * Opções.
         */

        {"EngineName",        NULL,}, // Sequência de desenhos.
        {"EngineDir",         NULL,}, // Sequência de desenhos.
        {"EngineCommand",     NULL,}, // Sequência de desenhos.
        {"Log",               NULL,}, // Verdadeiro/Falso.
        {"LogFile",           NULL,}, // Sequência de desenhos.
        {"Chess960",          NULL,}, // Verdadeiro/Falso.
        {"Resign",            NULL,}, // Verdadeiro/Falso.
        {"ResignMoves",       NULL,}, // Mover número.
        {"ResignScore",       NULL,}, // Centipeões.
        {"MateScore",         NULL,}, // Centipeões.
        {"Book",              NULL,}, // Verdadeiro/Falso.
        {"BookFile",          NULL,}, // Sequência de desenhos.
        {"BookRandom",        NULL,}, // Verdadeiro/Falso.
        {"BookWorst",         NULL,}, // Verdadeiro/Falso.
        {"BookLearn",         NULL,}, // Verdadeiro/Falso.
        {"KibitzMove",        NULL,}, // Verdadeiro/Falso.
        {"KibitzPV",          NULL,}, // Verdadeiro/Falso.
        {"KibitzCommand",     NULL,}, // Sequência de desenhos.
        {"KibitzDelay",       NULL,}, // Segundos.
        {"ShowPonder",        NULL,}, // Verdadeiro/Falso.

        /**
         * Soluções alternativas.
         */

        {"UCIVersion",        NULL,}, // 1-
        {"CanPonder",         NULL,}, // true/false
        {"SyncStop",          NULL,}, // true/false
        {"PromoteWorkAround", NULL,}, // true/false

        /**
         * Antes:
         *     { "", NULL, },
         *
         * Agora:
         */

        {NULL,                NULL,},
    };

    /**
     * Protótipos.
     */

    /**
     *
     */
    static option_t * option_find(const char var[]);

    /**
     * Funções.
     */

    /**
     *
     */
    void option_init()
    {
        /**
         * Arquivo de opção.
         */

        const char optionName[] = "gnuchess.ini";
        char optionFile[MaxFileNameSize + 1];
        FILE *of;

        if ((of = fopen(optionName, "r")) != NULL)
        {
            fclose(of);
            strcpy(optionFile, "");
        } else
        {
            strcpy(optionFile, compute_pkgdatadir());
            strcat(optionFile, "/");
        }

        strcat(optionFile, optionName);

        /**
         * Opções.
         */

        option_set("OptionFile",    optionFile);
        option_set("EngineName",    "Xadrez");
        option_set("EngineDir",     ".");
        option_set("EngineCommand", "<empty>");
        option_set("Log",           "false");
        option_set("LogFile",       "adapter.log");
        option_set("Chess960",      "false");
        option_set("Resign",        "false");
        option_set("ResignMoves",   "3");
        option_set("ResignScore",   "600");
        option_set("MateScore",     "10000");
        option_set("Book",          "false");
        option_set("BookFile",      "book.bin");
        option_set("BookRandom",    "true");
        option_set("BookWorst",     "false");
        option_set("BookLearn",     "false");
        option_set("KibitzMove",    "false");
        option_set("KibitzPV",      "false");
        option_set("KibitzCommand", "tellall");
        option_set("KibitzDelay",   "5");
        option_set("ShowPonder",    "true");

        /**
         * Soluções alternativas.
         */

        option_set("UCIVersion",       "2");
        option_set("CanPonder",        "false");
        option_set("SyncStop",         "false");
        option_set("PromoteWorkAround","false");

        /**
         * option_set("", "");
         */
    }

    /**
     *
     */
    bool option_set(const char var[], const char val[])
    {
        option_t * opt;

        ASSERT(var != NULL);
        ASSERT(val != NULL);
        opt = option_find(var);

        if (opt == NULL)
        {
            return false;
        }

        my_string_set(&opt->val, val);

        if (UseDebug)
        {
            my_log("POLYGLOT OPTION SET \"%s\" -> \"%s\"\n", opt->var, opt->val);
        }

        return true;
    }

    /**
     *
     */
    const char * option_get(const char var[])
    {
        option_t * opt;

        ASSERT(var != NULL);
        opt = option_find(var);

        if (opt == NULL)
        {
            my_fatal("option_get(): unknown option \"%s\"\n", var);
        }

        if (UseDebug)
        {
            my_log("POLYGLOT OPTION GET \"%s\" -> \"%s\"\n", opt->var, opt->val);
        }

        return opt->val;
    }

    /**
     *
     */
    bool option_get_bool(const char var[])
    {
        const char * val;

        val = option_get(var);

        if (false)
        {
        } else if (my_string_case_equal(val, "true") || my_string_case_equal(val, "yes") || my_string_equal(val, "1"))
        {
            return true;
        } else if (my_string_case_equal(val, "false") || my_string_case_equal(val, "no") || my_string_equal(val, "0"))
        {
            return false;
        }

        ASSERT(false);

       return false;
    }

    /**
     *
     */
    double option_get_double(const char var[])
    {
        const char * val;
        val = option_get(var);

        return atof(val);
    }

    /**
     *
     */
    int option_get_int(const char var[])
    {
        const char * val;
        val = option_get(var);

        return atoi(val);
    }

    /**
     *
     */
    const char * option_get_string(const char var[])
    {
        const char * val;
        val = option_get(var);

       return val;
    }

    /**
     *
     */
    static option_t * option_find(const char var[])
    {
        option_t * opt;
        ASSERT(var != NULL);

        for (opt = &Option[0]; opt->var != NULL; opt++)
        {
            if (my_string_case_equal(opt->var, var))
            {
                return opt;
            }
        }

        return NULL;
    }
}
