/**
 * square.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */

#ifndef SQUARE_H
#define SQUARE_H

    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int SquareNb = 16 * 12;

        /**
         *
         */
        const int FileA = 0;

        /**
         *
         */
        const int FileB = 1;

        /**
         *
         */
        const int FileC = 2;

        /**
         *
         */
        const int FileD = 3;

        /**
         *
         */
        const int FileE = 4;

        /**
         *
         */
        const int FileF = 5;

        /**
         *
         */
        const int FileG = 6;

        /**
         *
         */
        const int FileH = 7;

        /**
         *
         */
        const int Rank1 = 0;

        /**
         *
         */
        const int Rank2 = 1;

        /**
         *
         */
        const int Rank3 = 2;

        /**
         *
         */
        const int Rank4 = 3;

        /**
         *
         */
        const int Rank5 = 4;

        /**
         *
         */
        const int Rank6 = 5;

        /**
         *
         */
        const int Rank7 = 6;

        /**
         *
         */
        const int Rank8 = 7;

        /**
         *
         */
        const int SquareNone = 0;

        /**
         *
         */
        const int A1 = 0x24;

        /**
         *
         */
        const int B1 = 0x25;

        /**
         *
         */
        const int C1 = 0x26;

        /**
         *
         */
        const int D1 = 0x27;

        /**
         *
         */
        const int E1 = 0x28;

        /**
         *
         */
        const int F1 = 0x29;

        /**
         *
         */
        const int G1 = 0x2A;

        /**
         *
         */
        const int H1 = 0x2B;

        /**
         *
         */
        const int A2 = 0x34;

        /**
         *
         */
        const int B2 = 0x35;

        /**
         *
         */
        const int C2 = 0x36;

        /**
         *
         */
        const int D2 = 0x37;

        /**
         *
         */
        const int E2 = 0x38;

        /**
         *
         */
        const int F2 = 0x39;

        /**
         *
         */
        const int G2 = 0x3A;

        /**
         *
         */
        const int H2 = 0x3B;

        /**
         *
         */
        const int A3 = 0x44;

        /**
         *
         */
        const int B3 = 0x45;

        /**
         *
         */
        const int C3 = 0x46;

        /**
         *
         */
        const int D3 = 0x47;

        /**
         *
         */
        const int E3 = 0x48;

        /**
         *
         */
        const int F3 = 0x49;

        /**
         *
         */
        const int G3 = 0x4A;

        /**
         *
         */
        const int H3 = 0x4B;

        /**
         *
         */
        const int A4 = 0x54;

        /**
         *
         */
        const int B4 = 0x55;

        /**
         *
         */
        const int C4 = 0x56;

        /**
         *
         */
        const int D4 = 0x57;

        /**
         *
         */
        const int E4 = 0x58;

        /**
         *
         */
        const int F4 = 0x59;

        /**
         *
         */
        const int G4 = 0x5A;

        /**
         *
         */
        const int H4 = 0x5B;

        /**
         *
         */
        const int A5 = 0x64;

        /**
         *
         */
        const int B5 = 0x65;

        /**
         *
         */
        const int C5 = 0x66;

        /**
         *
         */
        const int D5 = 0x67;

        /**
         *
         */
        const int E5 = 0x68;

        /**
         *
         */
        const int F5 = 0x69;

        /**
         *
         */
        const int G5 = 0x6A;

        /**
         *
         */
        const int H5 = 0x6B;

        /**
         *
         */
        const int A6 = 0x74;

        /**
         *
         */
        const int B6 = 0x75;

        /**
         *
         */
        const int C6 = 0x76;

        /**
         *
         */
        const int D6 = 0x77;

        /**
         *
         */
        const int E6 = 0x78;

        /**
         *
         */
        const int F6 = 0x79;

        /**
         *
         */
        const int G6 = 0x7A;

        /**
         *
         */
        const int H6 = 0x7B;

        /**
         *
         */
        const int A7 = 0x84;

        /**
         *
         */
        const int B7 = 0x85;

        /**
         *
         */
        const int C7 = 0x86;

        /**
         *
         */
        const int D7 = 0x87;

        /**
         *
         */
        const int E7 = 0x88;

        /**
         *
         */
        const int F7 = 0x89;

        /**
         *
         */
        const int G7 = 0x8A;

        /**
         *
         */
        const int H7 = 0x8B;

        /**
         *
         */
        const int A8 = 0x94;

        /**
         *
         */
        const int B8 = 0x95;

        /**
         *
         */
        const int C8 = 0x96;

        /**
         *
         */
        const int D8 = 0x97;

        /**
         *
         */
        const int E8 = 0x98;

        /**
         *
         */
        const int F8 = 0x99;

        /**
         *
         */
        const int G8 = 0x9A;

        /**
         *
         */
        const int H8 = 0x9B;

        /**
         *
         */
        const int Dark = 0;

        /**
         *
         */
        const int Light = 1;

        /**
         * Funções.
         */

        /**
         *
         */
        extern void square_init();

        /**
         *
         */
        extern bool square_is_ok(int square);

        /**
         *
         */
        extern int  square_make(int file, int rank);

        /**
         *
         */
        extern int  square_file(int square);

        /**
         *
         */
        extern int  square_rank(int square);

        /**
         *
         */
        extern int  square_side_rank(int square, int colour);

        /**
         *
         */
        extern int  square_from_64(int square);

        /**
         *
         */
        extern int  square_to_64(int square);

        /**
         *
         */
        extern bool square_is_promote(int square);

        /**
         *
         */
        extern int  square_ep_dual(int square);

        /**
         *
         */
        extern int  square_colour(int square);

        /**
         *
         */
        extern bool char_is_file(int c);

        /**
         *
         */
        extern bool char_is_rank(int c);

        /**
         *
         */
        extern int  file_from_char(int c);

        /**
         *
         */
        extern int  rank_from_char(int c);

        /**
         *
         */
        extern int  file_to_char(int file);

        /**
         *
         */
        extern int  rank_to_char(int rank);

        /**
         *
         */
        extern bool square_to_string(int square, char string[], int size);

        /**
         *
         */
        extern int  square_from_string(const char string[]);
    }

#endif
