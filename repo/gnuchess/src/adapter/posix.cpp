/**
 * posix.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <ctime>

/**
 * A maça precisa deste primeiro.
 */
#include <sys/time.h>

#include <sys/resource.h>
#include <sys/types.h>
#include <unistd.h>

#include "posix.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Protótipos.
     */

    /**
     *
     */
    static double duration(const struct timeval *t);

    /**
     * Funções.
     */

    /**
     *
     */
    bool input_available()
    {
        int val;
        fd_set set[1];
        struct timeval time_val[1];

        FD_ZERO(set);
        FD_SET(STDIN_FILENO, set);

        time_val->tv_sec = 0;
        time_val->tv_usec = 0;
        val = select(STDIN_FILENO + 1, set, NULL, NULL, time_val);

        if (val == -1)
        {
            my_fatal("input_available(): select(): %s\n", strerror(errno));
        }

        return val != 0;
    }

    /**
     *
     */
    double now_real()
    {
        struct timeval tv[1];
        struct timezone tz[1];

        /**
         *
         */
        tz->tz_minuteswest = 0;

        /**
         * Não foi declarado aqui ainda.
         */
        tz->tz_dsttime = 0;

        if (gettimeofday(tv, tz) == -1)
        {
            my_fatal("now_real(): gettimeofday(): %s\n", strerror(errno));
        }

        return duration(tv);
    }

    /**
     *
     */
    double now_cpu()
    {
        struct rusage ru[1];

        if (getrusage(RUSAGE_SELF, ru) == -1)
        {
            my_fatal("now_cpu(): getrusage(): %s\n", strerror(errno));
        }

        return duration(&ru->ru_utime);
    }

    /**
     *
     */
    static double duration(const struct timeval *tv)
    {
        return tv->tv_sec + tv->tv_usec * 1E-6;
    }
}
