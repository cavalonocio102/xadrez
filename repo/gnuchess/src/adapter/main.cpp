/**
 * main.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include <pthread.h>
#include <cerrno>
#include <csignal>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "adapter.h"
#include "attack.h"
#include "board.h"
#include "book.h"
#include "book_make.h"
#include "book_merge.h"
#include "engine.h"
#include "epd.h"
#include "fen.h"
#include "hash.h"
#include "list.h"
#include "main.h"
#include "move.h"
#include "move_gen.h"
#include "option.h"
#include "piece.h"
#include "search.h"
#include "square.h"
#include "uci.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Constantes.
     */

    /**
     *
     */
    static const char * const Version = "1.4";

    /**
     * Verdadeiro aqui.
     */
    static const bool BlockSignal = false;

    /**
     *
     */
    static const int SearchDepth = 63;

    /**
     *
     */
    static const double SearchTime = 3600.0;

    /**
     *
     */
    static const int StringSize = 4096;

    /**
     * Variáveis.
     */

    /**
     *
     */
    static bool Init;

    /**
     *
     */
    extern unsigned int HashSize;

    /**
     *
     */
    pthread_mutex_t adapter_init_mutex;

    /**
     *
     */
    pthread_cond_t adapter_init_cond;

    /**
     * Protótipos.
     */

    /**
     *
     */
    static void parse_option();

    /**
     *
     */
    static bool parse_line(char line[], char * * name_ptr, char * * value_ptr);

    /**
     *
     */
    static void stop_search();

    /**
     * Funções.
     */

    /**
     * Aviso: Adaptador principal.
     */
    int main_adapter(int argc, char * argv[])
    {
        /**
         * Inicializado.
         */
        Init = false;

        if (BlockSignal)
        {
            signal(SIGINT, SIG_IGN);
            signal(SIGTERM, SIG_IGN);
            signal(SIGPIPE, SIG_IGN);
        }

        util_init();
        option_init();
        square_init();
        piece_init();
        attack_init();
        hash_init();
        my_random_init();

        /**
         * Construção do livro.
         */
        if (argc >= 2 && my_string_equal(argv[1], "make-book"))
        {
            book_make(argc, argv);
            return EXIT_SUCCESS;
        }

        if (argc >= 2 && my_string_equal(argv[1],"merge-book"))
        {
            book_merge(argc, argv);
            return EXIT_SUCCESS;
        }

        /**
         * Opções de leitura.
         */
        if (argc == 2)
        {
            /**
             * Para compatibilidade.
             */
            option_set("OptionFile", argv[1]);
        }

        /**
         * Também inicializa um novo processo.
         */
        parse_option();

        /**
         * Aviso: Verificação.
         */
        if (argc >= 2 && my_string_equal(argv[1], "epd-test"))
        {
            epd_test(argc, argv);

            return EXIT_SUCCESS;
        }

        /**
         * Abrindo o livro.
         */
        book_clear();

        if (option_get_bool("Book"))
        {
            int mode = BookReadOnly;
            if (option_get_bool("BookLearn"))
            {
                mode = BookReadWrite;
            }

            book_open(option_get_string("BookFile"), mode);
        }

        /**
         * Aviso: Adaptador sinalizador inicializado.
         */
        pthread_mutex_lock(&adapter_init_mutex);
        pthread_cond_signal(&adapter_init_cond);
        pthread_mutex_unlock(&adapter_init_mutex);

        /**
         * Aviso: Adaptador.
         */
        adapter_loop();
        engine_send(Engine, "quit");
        engine_close(Engine);

        return EXIT_SUCCESS;
    }

    /**
     *
     */
    static void parse_option()
    {
        const char * file_name;
        FILE * file;

        char line[256];
        char * name, * value;

        file_name = option_get_string("OptionFile");
        file = fopen(file_name, "r");

        if (file == NULL)
        {
            /**
             * Falha no software, fechando aplicação...
             */
            my_fatal("Não é possível abrir o arquivo \"%s\": %s - usando Especificação\n", file_name, strerror(errno));
        }

        /**
         * Opções.
         */
        if (file != NULL)
        {
            while (true)
            {
                if (!my_file_read_line(file, line, 256))
                {
                    my_fatal("parse_option(): Falta a seção de [Especificação]\n");
                }

                if (my_string_case_equal(line, "[engine]"))
                {
                    break;
                }

                if (parse_line(line, &name, &value))
                {
                    option_set(name, value);
                }
            }
        }

        if (option_get_bool("Log"))
        {
            my_log_open(option_get_string("LogFile"));
        }

        my_log("BABEL *** INICIAR ***\n");
        my_log("BABEL Arquivo INI \"%s\"\n", file_name);

        /**
         * Opções de especificação.
         */
        engine_open(Engine);

        /**
         * A especificação foi inicializada.
         */
        Init = true;
        uci_open(Uci, Engine);

        if (file != NULL)
        {
            while (my_file_read_line(file, line, 256))
            {
                if (line[0] == '[')
                {
                    my_fatal("parse_option(): seção desconhecida %s\n", line);
                }

                if (parse_line(line, &name, &value))
                {
                    if (strcmp(name,"Hash") == 0)
                    {
                        sscanf(value, "%d", &HashSize);
                    }

                    uci_send_option(Uci, name, "%s", value);
                }
            }
        }

        uci_send_isready(Uci);
        if (file != NULL)
        {
            fclose(file);
        }

        if (my_string_equal(option_get_string("EngineName"), "<empty>"))
        {
            option_set("EngineName", Uci->name);
        }
    }

    /**
     *
     */
    static bool parse_line(char line[], char * * name_ptr, char * * value_ptr)
    {
        char * ptr;
        char * name, * value;

        ASSERT(line != NULL);
        ASSERT(name_ptr != NULL);
        ASSERT(value_ptr != NULL);

        /**
         * Evitar comentários.
         */

        ptr = strchr(line, ';');
        if (ptr != NULL)
        {
            *ptr = '\0';
        }

        ptr = strchr(line,'#');
        if (ptr != NULL)
        {
            *ptr = '\0';
        }

        /**
         * Dividir linha em partes iguais '='.
         */
        ptr = strchr(line, '=');
        if (ptr == NULL)
        {
            return false;
        }

        name = line;
        value = ptr + 1;

        /**
         * Nome de faxina.
         */
        while (*name == ' ')
        {
            /**
             * Remover espaços à esquerda.
             */
            name++;
        }

        while (ptr > name && ptr[-1] == ' ')
        {
            /**
             * Remover espaços à direita.
             */
            ptr--;
        }

        *ptr = '\0';
        if (*name == '\0')
        {
            return false;
        }

        /**
         * Valor da faxina.
         */

        /**
         * Endereço para a conclusão do serviço.
         */
        ptr = &value[strlen(value)];

        /**
         *
         */
        while (*value == ' ')
        {
            /**
             * Evite espaços à esquerda.
             */
            value++;
        }

        while (ptr > value && ptr[-1] == ' ')
        {
            /**
             * Evite espaços à direita.
             */
            ptr--;
        }

        *ptr = '\0';

        if (*value == '\0')
        {
            return false;
        }

        *name_ptr = name;
        *value_ptr = value;

        return true;
    }

    /**
     * Fechar.
     */
    void quit()
    {
        char string[StringSize];

        my_log("BABEL *** FECHAR ***\n");

        if (Init)
        {
            stop_search();
            engine_send(Engine, "quit");
            pthread_exit(NULL);

            /**
             * Espere a conclusão da especificação.
             */
            while (true)
            {
                /**
                 * Chama exit() (Função fechar) ao receber EOF.
                 */
                engine_get(Engine, string, StringSize);
            }

            uci_close(Uci);
        }

        /**
         * exit(EXIT_SUCCESS);
         */
    }

    /**
     *
     */
    static void stop_search()
    {
        if (Init && Uci->searching)
        {
            ASSERT(Uci->searching);
            ASSERT(Uci->pending_nb >= 1);
            my_log("BABEL PARAR BUSCADOR\n");

            /**
             * engine_send(Engine, "stop");
             * Uci->searching = false;
             */

            if (option_get_bool("SyncStop"))
            {
                uci_send_stop_sync(Uci);
            } else
            {
                uci_send_stop(Uci);
            }
        }
    }
}
