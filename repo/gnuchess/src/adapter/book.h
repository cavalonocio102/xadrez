/**
 * book.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef BOOK_H
#define BOOK_H

    #include "board.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int BookReadOnly = 1;

        /**
         *
         */
        const int BookReadWrite = 2;

        /**
         * Funções.
         */

        /**
         *
         */
        extern void book_clear();

        /**
         *
         */
        extern void book_open(const char file_name[], int mode);

        /**
         *
         */
        extern void book_close();

        /**
         *
         */
        extern bool is_in_book(const board_t * board);

        /**
         *
         */
        extern int  book_move(const board_t * board, bool random);

        /**
         *
         */
        extern int  book_move(const board_t * board, bool random, bool worst);

        /**
         *
         */
        extern void book_disp(const board_t * board);

        /**
         *
         */
        extern void book_learn_move(const board_t * board, int move, int result);

        /**
         *
         */
        extern void book_flush();
    }

#endif
