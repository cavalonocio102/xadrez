/**
 * io.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef IO_H
#define IO_H

    #include "util.h"

    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int BufferSize = 16384;

        /**
         * Tipos.
         */

        /**
         *
         */
        struct io_t
        {
            /**
             *
             */
            int in_fd;

            /**
             *
             */
            int out_fd;

            /**
             *
             */
            const char * name;

            /**
             *
             */
            bool in_eof;

            /**
             *
             */
            sint32 in_size;

            /**
             *
             */
            sint32 out_size;

            /**
             *
             */
            char in_buffer[BufferSize];

            /**
             *
             */
            char out_buffer[BufferSize];
        };

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool io_is_ok(const io_t * io);

        /**
         *
         */
        extern void io_init(io_t * io);

        /**
         *
         */
        extern void io_close(io_t * io);

        /**
         *
         */
        extern void io_get_update(io_t * io);

        /**
         *
         */
        extern bool io_line_ready(const io_t * io);

        /**
         *
         */
        extern bool io_get_line(io_t * io, char string[], int size);

        /**
         *
         */
        extern void io_send(io_t * io, const char format[], ...);

        /**
         *
         */
        extern void io_send_queue(io_t * io, const char format[], ...);
    }

#endif
