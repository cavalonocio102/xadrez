/**
 * adapter.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */

#include <cerrno>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <sys/select.h>
#include <sys/types.h> // A maça requer essa linha.
#include <unistd.h>

#include "adapter.h"
#include "board.h"
#include "book.h"
#include "book_make.h"
#include "colour.h"
#include "engine.h"
#include "fen.h"
#include "game.h"
#include "io.h"
#include "line.h"
#include "main.h"
#include "move.h"
#include "move_do.h"
#include "move_legal.h"
#include "option.h"
#include "parse.h"
#include "san.h"
#include "uci.h"
#include "util.h"
#include "components.h"


/**
 *
 */
namespace adapter
{
    /**
     * Constantes.
     */

    /**
     *
     */
    static const bool UseDebug = false;

    /**
     *
     */
    static const bool DelayPong = false;

    /**
     *
     */
    static const int StringSize = 4096;

    /**
     * Tipos.
     */

    /**
     *
     */
    struct xboard_t
    {
        /**
         *
         */
        io_t io[1];
    };

    /**
     *
     */
    struct state_t
    {
        /**
         *
         */
        int state;

        /**
         *
         */
        bool computer[ColourNb];

        /**
         *
         */
        int exp_move;

        /**
         *
         */
        int resign_nb;

        /**
         *
         */
        my_timer_t timer[1];
    };

    /**
     *
     */
    struct xb_t
    {
        /**
         *
         */
        bool analyse;

        /**
         *
         */
        bool computer;

        /**
         *
         */
        const char * name;

        /**
         *
         */
        bool ics;

        /**
         * "novo" é uma palavra-chave C++.
         */
        bool new_hack;

        /**
         *
         */
        bool ponder;

        /**
         *
         */
        int ping;

        /**
         *
         */
        bool post;

        /**
         *
         */
        int proto_ver;

        /**
         *
         */
        bool result;

        /**
         *
         */
        int mps;

        /**
         *
         */
        double base;

        /**
         *
         */
        double inc;

        /**
         *
         */
        bool time_limit;

        /**
         *
         */
        double time_max;

        /**
         *
         */
        bool depth_limit;

        /**
         *
         */
        int depth_max;

        /**
         *
         */
        double my_time;

        /**
         *
         */
        double opp_time;
    };

    /**
     *
     */
    enum dummy_state_t
    {
        WAIT,
        THINK,
        PONDER,
        ANALYSE
    };

    /**
     * Variáveis.
     */

    /**
     *
     */
    static xboard_t XBoard[1];

    /**
     *
     */
    static state_t State[1];

    /**
     *
     */
    static xb_t XB[1];

    /**
     *
     */
    extern unsigned int HashSize;

    /**
     * Protótipos.
     */

    /**
     *
     */
    static void adapter_step();

    /**
     *
     */
    static void xboard_step();

    /**
     *
     */
    static void engine_step();

    /**
     *
     */
    static void comp_move(int move);

    /**
     *
     */
    static void move_step(int move);

    /**
     *
     */
    static void board_update();

    /**
     *
     */
    static void mess();

    /**
     *
     */
    static void no_mess(int move);

    /**
     *
     */
    static void search_update();

    /**
     *
     */
    static void search_clear();

    /**
     *
     */
    static bool active();

    /**
     *
     */
    static bool ponder();

    /**
     *
     */
    static bool ponder_move_is_ok(int ponder_move);

    /**
     *
     */
    static void stop_search();

    /**
     * static void quit();
     */

    /**
     *
     */
    static void send_board(int extra_move);

    /**
     *
     */
    static void send_pv();

    /**
     *
     */
    static void xboard_get(xboard_t * xboard, char string[], int size);

    /**
     *
     */
    static void xboard_send(xboard_t * xboard, const char format[], ...);

    /**
     *
     */
    static void learn(int result);

    /**
     * Funções.
     */

    /**
     * adapter_loop()
     */

    /**
     *
     */
    void adapter_loop()
    {
        /**
         * Iniciar.
         */

        game_clear(Game);

        /**
         * Estado.
         */

        State->state = WAIT;
        State->computer[White] = false;
        State->computer[Black] = true;
        State->exp_move = MoveNone;
        State->resign_nb = 0;
        my_timer_reset(State->timer);

        /**
         * xboard.
         */

        XBoard->io->in_fd = pipefd_f2a[0];
        XBoard->io->out_fd = pipefd_a2f[1];
        XBoard->io->name = "XBOARD";
        io_init(XBoard->io);

        XB->analyse = false;
        XB->computer = false;
        XB->name = NULL;
        my_string_set(&XB->name,"<empty>");

        XB->ics = false;
        XB->new_hack = true;
        XB->ping = -1;
        XB->ponder = true;
        XB->post = false;
        XB->proto_ver = 1;
        XB->result = false;

        XB->mps = 0;
        XB->base = 300.0;
        XB->inc = 0.0;

        XB->time_limit = false;
        XB->time_max = 5.0;

        XB->depth_limit = false;
        XB->depth_max = 127;

        XB->my_time = 300.0;
        XB->opp_time = 300.0;

        /**
         * Ciclos
         */

        /**
         * Enquanto...
         */
        while (true)
        {
            adapter_step();

            /**
             * Caso contrário, às vezes o programa falhará ao gastar muito tempo
             * resolvendo uma posição.
             *
             */
            sleep(0);
        }
    }

    /**
     *
     */
    static void adapter_step()
    {
        fd_set set[1];
        int fd_max;
        int val;

        /**
         * Processar linhas em buffer.
         */

        while (io_line_ready(XBoard->io))
        {
            /**
             * Processar linhas xboard disponíveis.
             */
            xboard_step();
        }

        while (io_line_ready(Engine->io))
        {
            /**
             * Processar linhas de especificação disponíveis.
             */
            engine_step();
        }

        /**
         * Iniciar.
         */

        FD_ZERO(set);

        /**
         * Fixme: XXX.
         */
        fd_max = -1;

        /**
         * Adicionar entrada xboard.
         */

        ASSERT(XBoard->io->in_fd >= 0);
        FD_SET(XBoard->io->in_fd, set);

        if (XBoard->io->in_fd > fd_max)
        {
            fd_max = XBoard->io->in_fd;
        }

        /**
         * Adicionar entrada da especificação.
         */

        ASSERT(Engine->io->in_fd >= 0);
        FD_SET(Engine->io->in_fd, set);

        if (Engine->io->in_fd > fd_max)
        {
            fd_max = Engine->io->in_fd;
        }

        /**
         * Aguarde algo para ler (sem tempo limite).
         */

        ASSERT(fd_max >= 0);

        val = select(fd_max + 1, set, NULL, NULL, NULL);

        if (val == -1 && errno != EINTR)
        {
            my_fatal("adapter_step(): select(): %s\n", strerror(errno));
        }

        if (val > 0)
        {
            if (FD_ISSET(XBoard->io->in_fd, set))
            {
                /**
                 * Leia algumas entradas do xboard.
                 */
                io_get_update(XBoard->io);
            }

            if (FD_ISSET(Engine->io->in_fd, set))
            {
                /**
                 * ler alguma entrada da especificação.
                 */
                io_get_update(Engine->io);
            }
        }
    }

    /**
     *
     */
    static void xboard_step()
    {
        char string[StringSize];
        int move;
        char move_string[256];
        board_t board[1];

        xboard_get(XBoard, string, StringSize);

        /**
         * ACL - Lista de controle de acesso e permissão.
         */
        if (false)
        {
        } else if (match(string, "accepted *"))
        {
            /**
             * Pular.
             */
        } else if (match(string, "analyze"))
        {
            State->computer[White] = false;
            State->computer[Black] = false;

            XB->analyse = true;
            XB->new_hack = false;
            ASSERT(!XB->result);
            XB->result = false;

            mess();
        } else if (match(string, "bk"))
        {
            if (option_get_bool("Book"))
            {
                game_get_board(Game, board);
                book_disp(board);
            }
        } else if (match(string, "black"))
        {
            if (colour_is_black(game_turn(Game)))
            {
                State->computer[White] = true;
                State->computer[Black] = false;

                XB->new_hack = true;
                XB->result = false;

                mess();
            }
        } else if (match(string, "computer"))
        {
            XB->computer = true;
        } else if (match(string, "draw"))
        {
            /**
             * Pular.
             */
        } else if (match(string, "easy"))
        {
            XB->ponder = false;

            mess();
        } else if (match(string, "edit"))
        {
            /**
             * Recusar.
             */
            xboard_send(XBoard, "Falha (Entrada desconhecida): %s", string);
        } else if (match(string, "exit"))
        {
            State->computer[White] = false;
            State->computer[Black] = false;

            XB->analyse = false;

            mess();
        } else if (match(string, "force"))
        {
            State->computer[White] = false;
            State->computer[Black] = false;

            mess();
        } else if (match(string, "go"))
        {
            State->computer[game_turn(Game)] = true;
            State->computer[colour_opp(game_turn(Game))] = false;

            XB->new_hack = false;
            ASSERT(!XB->result);
            XB->result = false;

            mess();
        } else if (match(string, "hard"))
        {
            /**
             * Alta Performance - "XXX":
             *     Aqui temos a sensação de sentir uma leve coceira no
             *     próximo dia...
             *
             * Observação:
             *     Ciência = Base 4.
             */
            XB->ponder = true;

            mess();
        } else if (match(string, "hint"))
        {
            if (option_get_bool("Book"))
            {
                game_get_board(Game, board);
                move = book_move(board, false);

                if (move != MoveNone && move_is_legal(move, board))
                {
                    move_to_san(move, board, move_string, 256);
                    xboard_send(XBoard, "Hint: %s", move_string);
                }
            }
        } else if (match(string, "ics *"))
        {
            XB->ics = true;
        } else if (match(string, "level * *:* *"))
        {
            XB->mps  = atoi(Star[0]);
            XB->base = double(atoi(Star[1])) * 60.0 + double(atoi(Star[2]));
            XB->inc  = double(atoi(Star[3]));
        } else if (match(string, "level * * *"))
        {
            XB->mps  = atoi(Star[0]);
            XB->base = double(atoi(Star[1])) * 60.0;
            XB->inc  = double(atoi(Star[2]));
        } else if (match(string, "name *"))
        {
            my_string_set(&XB->name, Star[0]);
        } else if (match(string, "new"))
        {
            my_log("POLYGLOT NEW GAME\n");
            option_set("Chess960", "false");
            game_clear(Game);

            if (XB->analyse)
            {
                State->computer[White] = false;
                State->computer[Black] = false;
            } else
            {
                State->computer[White] = false;
                State->computer[Black] = true;
            }

            XB->new_hack = true;
            XB->result = false;
            XB->depth_limit = false;
            XB->computer = false;

            my_string_set(&XB->name, "<empty>");
            board_update();
            mess();

            uci_send_ucinewgame(Uci);
        } else if (match(string, "nopost"))
        {
            XB->post = false;
        } else if (match(string, "otim *"))
        {
            XB->opp_time = double(atoi(Star[0])) / 100.0;

            if (XB->opp_time < 0.0)
            {
                XB->opp_time = 0.0;
            }
        } else if (match(string, "pause"))
        {
            /**
             * Recusar.
             */

            xboard_send(XBoard,"Error (unknown command): %s", string);
        } else if (match(string, "ping *"))
        {
            /**
             * Responder somente após um movimento da especificação.
             */

            if (DelayPong)
            {
                if (XB->ping >= 0)
                {
                    /**
                     *
                     */
                    xboard_send(XBoard, "pong %d", XB->ping);
                }

                XB->ping = atoi(Star[0]);
                uci_send_isready(Uci);
            } else
            {
                ASSERT(XB->ping == -1);
                xboard_send(XBoard, "pong %s", Star[0]);
            }
        } else if (match(string, "playother"))
        {
            State->computer[game_turn(Game)] = false;
            State->computer[colour_opp(game_turn(Game))] = true;

            XB->new_hack = false;
            ASSERT(!XB->result);
            XB->result = false;

            mess();
        } else if (match(string, "post"))
        {
            XB->post = true;
        } else if (match(string, "protover *"))
        {
            XB->proto_ver = atoi(Star[0]);
            ASSERT(XB->proto_ver >= 2);

            xboard_send(XBoard, "feature done=0");
            xboard_send(XBoard, "feature analyze=1");
            xboard_send(XBoard, "feature colors=0");
            xboard_send(XBoard, "feature draw=1");
            xboard_send(XBoard, "feature ics=1");
            xboard_send(XBoard, "feature myname=\"%s\"", option_get_string("EngineName"));
            xboard_send(XBoard, "feature name=1");
            xboard_send(XBoard, "feature pause=0");
            xboard_send(XBoard, "feature ping=1");
            xboard_send(XBoard, "feature playother=1");
            xboard_send(XBoard, "feature reuse=1");
            xboard_send(XBoard, "feature san=0");
            xboard_send(XBoard, "feature setboard=1");
            xboard_send(XBoard, "feature sigint=0");
            xboard_send(XBoard, "feature sigterm=0");
            xboard_send(XBoard, "feature time=1");
            xboard_send(XBoard, "feature usermove=1");

            if (uci_option_exist(Uci, "UCI_Chess960"))
            {
                xboard_send(XBoard, "feature variants=\"normal,fischerandom\"");
            } else
            {
                xboard_send(XBoard, "feature variants=\"normal\"");
            }

            if (Uci->ready)
            {
                xboard_send(XBoard, "feature done=1");
            }

            /**
             * Caso contrário, "feature done=1" será enviado quando o
             * mecanismo estiver pronto.
             */
        } else if (match(string, "quit"))
        {
            my_log("POLYGLOT *** \"quit\" from XBoard ***\n");
            quit();
        } else if (match(string, "random"))
        {
            /**
             * Ignorar.
             */
        } else if (match(string, "rating * *"))
        {
            /**
             * Ignorar.
             */
        } else if (match(string, "remove"))
        {
            if (game_pos(Game) >= 2)
            {
                game_goto(Game, game_pos(Game) - 2);

                ASSERT(!XB->new_hack);
                XB->new_hack = false;
                XB->result = false;

                board_update();
                mess();
            }
        } else if (match(string, "rejected *"))
        {
            /**
             * Ignorar.
             */
        } else if (match(string, "reset"))
        {
            /**
             * protover 3 ?
             */

            /**
             * Recusar.
             */

            xboard_send(XBoard,"Falha (Entrada desconhecida): %s", string);
        } else if (false
            || match(string, "result * {*}")
            || match(string, "result * {* }")
            || match(string, "result * { *}")
            || match(string, "result * { * }"))
        {
            /**
             *
             */
            my_log("POLYGLOT GAME END\n");
            XB->result = true;

            mess();

            /**
             * Aprendendo a fazer um bom "XXX" em uma nova biblioteca...
             */

            if (option_get_bool("Book") && option_get_bool("BookLearn"))
            {
                if (false)
                {
                } else if (my_string_equal(Star[0], "1-0"))
                {
                    /**
                     * XXX ?
                     */
                    learn(+1);
                } else if (my_string_equal(Star[0], "0-1"))
                {
                    /**
                     * XXX ?
                     */
                    learn(-1);
                } else if (my_string_equal(Star[0], "1/2-1/2"))
                {
                    learn(0);
                }
            }
        } else if (match(string, "resume"))
        {
            /**
             * Recusar.
             */

            xboard_send(XBoard,"Falha (Entrada desconhecida): %s", string);
        } else if (match(string, "sd *"))
        {
            XB->depth_limit = true;
            XB->depth_max = atoi(Star[0]);
        } else if (match(string, "setboard *"))
        {
            my_log("POLYGLOT FEN %s\n", Star[0]);

            if (!game_init(Game, Star[0]))
            {
                my_fatal("xboard_step(): bad FEN \"%s\"\n",Star[0]);
            }

            State->computer[White] = false;
            State->computer[Black] = false;

            XB->new_hack = true;
            XB->result = false;

            board_update();
            mess();
        } else if (match(string, "st *"))
        {
            XB->time_limit = true;
            XB->time_max = double(atoi(Star[0]));
        } else if (match(string, "time *"))
        {
            XB->my_time = double(atoi(Star[0])) / 100.0;

            if (XB->my_time < 0.0)
            {
                XB->my_time = 0.0;
            }
        } else if (match(string, "undo"))
        {
            if (game_pos(Game) >= 1)
            {
                game_goto(Game, game_pos(Game) - 1);

                ASSERT(!XB->new_hack);
                XB->new_hack = false;
                XB->result = false;

                board_update();
                mess();
            }
        } else if (match(string, "usermove *"))
        {
            game_get_board(Game, board);
            move = move_from_san(Star[0], board);

            if (move != MoveNone && move_is_legal(move, board))
            {
                XB->new_hack = false;
                ASSERT(!XB->result);
                XB->result = false;

                move_step(move);
                no_mess(move);
            } else
            {
                xboard_send(XBoard, "Illegal move: %s", Star[0]);
            }
        } else if (match(string, "variant *"))
        {
            if (my_string_equal(Star[0], "fischerandom"))
            {
                option_set("Chess960", "true");
            } else
            {
                option_set("Chess960", "false");
            }
        } else if (match(string, "white"))
        {
            if (colour_is_white(game_turn(Game)))
            {
                State->computer[White] = false;
                State->computer[Black] = true;

                XB->new_hack = true;
                XB->result = false;

                mess();
            }
        } else if (match(string, "xboard"))
        {
            /**
             * Ignorar.
             */
        } else if (match(string, "."))
        {
            /**
             * Analisar informações
             */

            if (State->state == ANALYSE)
            {
                ASSERT(Uci->searching);
                ASSERT(Uci->pending_nb >= 1);

                if (Uci->root_move != MoveNone && move_is_legal(Uci->root_move, Uci->board))
                {
                    move_to_san(Uci->root_move, Uci->board, move_string, 256);

                    xboard_send(XBoard, "stat01: %.0f %lld %d %d %d %s", Uci->time * 100.0, Uci->node_nb, Uci->depth, Uci->root_move_nb - (Uci->root_move_pos + 1), Uci->root_move_nb, move_string);
                } else
                {
                    /**
                     *
                     */
                    xboard_send(XBoard, "stat01: %.0f %lld %d %d %d", Uci->time * 100.0, Uci->node_nb, Uci->depth, 0, 0);
                }
            }
        } else if (match(string, "?"))
        {
            /**
             * Mexa-se agora.
             */

            if (State->state == THINK)
            {
                ASSERT(Uci->searching);
                ASSERT(Uci->pending_nb >= 1);

                /**
                 * Basta enviar "stop" para a especificação.
                 */

                if (Uci->searching)
                {
                    my_log("POLYGLOT STOP SEARCH\n");
                    engine_send(Engine, "stop");
                }
            }
        } else
        {
            /**
             * Comandos estendidos do frontend Xadrez.
             */

            if (match(string, "book *"))
            {
                /**
                 * Rotina book (dô Xadrez).
                 */

                /**
                 * Chamar a funcionalidade do livro do Poliglota Subcomandos:
                 *     ~> adicionar,
                 *     ~> ativar,
                 *     ~> desativar,
                 *     ~> preferir,
                 *     ~> aleatório,
                 *     ~> melhor,
                 *     ~> pior.
                 */
                char *my_argv[4];
                char *token;
                token = strtok(Star[0], " ");

                if (strcmp(token, "add") == 0)
                {
                    token = strtok( NULL, " " );
                    my_argv[0] = (char *)malloc(2);
                    my_argv[1] = (char *)malloc(10);
                    my_argv[2] = (char *)malloc(5);
                    my_argv[3] = (char *)malloc(strlen(token) + 1);

                    strcpy(my_argv[0], "g");
                    strcpy(my_argv[1], "make-book");
                    strcpy(my_argv[2], "-pgn");
                    strcpy(my_argv[3], token);
                    book_make(4, my_argv);
                } else if (strcmp(token, "on") == 0)
                {
                    option_set("Book", "true");
                    int mode = BookReadOnly;

                    if (option_get_bool("BookLearn"))
                    {
                        mode = BookReadWrite;
                    }

                    book_open(option_get_string("BookFile"), mode);
                } else if (strcmp(token, "off") == 0)
                {
                    option_set("Book", "false");
                } else if (strcmp(token, "best") == 0)
                {
                    option_set("BookRandom", "false");
                    option_set("BookWorst", "false");
                } else if (strcmp(token, "best") == 0)
                {
                    option_set("BookRandom", "true");
                    option_set("BookWorst", "false");
                } else if (strcmp(token, "worst") == 0)
                {
                    option_set("BookWorst", "true");
                }
            } else if (match(string,"hashon"))
            {
                /**
                 * hash na rotina (dô Xadrez).
                 */

                engine_send(Engine, "hashon");
            } else if (match(string, "hashoff"))
            {
                /**
                 * Desligar rotina de hash (dô Xadrez).
                 */

                engine_send(Engine, "hashoff");
            } else if (match(string, "memory"))
            {
                /**
                 * FIXME: Comando de memória intelectual (dô Xadrez).
                 */

                xboard_send(XBoard, "Current HashSize is %d MB", HashSize);
            } else if (match(string, "memory *"))
            {
                /**
                 * Rotina N (dô Xadrez).
                 */

                char *token;
                unsigned int memory;
                token = strtok(Star[0], " ");

                if (sscanf(token, "%d", &memory) == 1)
                {
                    HashSize = memory;
                    uci_send_option(Uci, "Hash", "%d", HashSize);

                    /**
                     * xboard_send(XBoard, "Current HashSize is %d MB", HashSize);
                     */
                }

                engine_send(Uci->engine, "uci");
            } else if (match(string, "nullon"))
            {
                /**
                 * Em chamadas void (dô Xadrez).
                 */

                uci_send_option(Uci, "NullMove Pruning", "%s", "Always");
            } else if (match(string, "nulloff"))
            {
                /**
                 * Desligar chamadas void (dô Xadrez).
                 */

                uci_send_option(Uci, "NullMove Pruning", "%s", "Never");
            } else
            {
                /**
                 * Rotina desconhecida, talvez um movimento ?
                 */

                game_get_board(Game, board);
                move = move_from_san(string, board);

                if (move != MoveNone && move_is_legal(move, board))
                {
                    XB->new_hack = false;
                    ASSERT(!XB->result);
                    XB->result = false;

                    move_step(move);
                    no_mess(move);
                } else if (move != MoveNone)
                {
                    xboard_send(XBoard, "Illegal move: %s", string);
                } else
                {
                    xboard_send(XBoard,"Falha (Rotina desconhecida): %s",string);
                }
            }
        }
    }

    /**
     *
     */
    static void engine_step()
    {
        char string[StringSize];
        int event;

        /**
         * Analisar linha UCI.
         */
        engine_get(Engine, string, StringSize);
        event = uci_parse(Uci, string);

        /**
         * Reagir a eventos...
         */
        if ((event & EVENT_READY) != 0)
        {
            /**
             * A especificação já está pronta.
             */
            if (!Uci->ready)
            {
                Uci->ready = true;

                if (XB->proto_ver >= 2)
                {
                    xboard_send(XBoard, "feature done=1");
                }
            }

            if (!DelayPong && XB->ping >= 0)
            {
                xboard_send(XBoard, "pong %d", XB->ping);
                XB->ping = -1;
            }
        }

        if ((event & EVENT_MOVE) != 0 && State->state == THINK)
        {
            /**
             * A especificação pode para fazer um movimento.
             */

            /**
             * FIXME: estime o tempo restante porque o XBoard não enviará !
             */
            my_timer_stop(State->timer);
            XB->my_time -= my_timer_elapsed_real(State->timer);
            XB->my_time += XB->inc;

            if (XB->mps != 0 && (game_move_nb(Game) + 1) % XB->mps == 0)
            {
                XB->my_time += XB->base;
            }

            if (XB->my_time < 0.0)
            {
                XB->my_time = 0.0;
            }

            /**
             * Jogar o movimento da especificação.
             */
            comp_move(Uci->best_move);
        }

        if ((event & EVENT_PV) != 0)
        {
            /**
             * A especificação enviou um novo PV.
             */
            send_pv();
        }
    }

    /**
     *
     */
    static void comp_move(int move)
    {
        board_t board[1];
        char string[256];

        ASSERT(move_is_ok(move));
        ASSERT(State->state == THINK);
        ASSERT(!XB->analyse);

        /**
         * Para atualizar o tempo e os nós.
         */
        send_pv();

        /**
         * Enviar o movimento.
         */
        game_get_board(Game, board);

        if (move_is_castle(move, board) && option_get_bool("Chess960"))
        {
            if (!move_to_san(move, board, string, 256))
            {
                /**
                 * O-O/O-O-O.
                 */
                my_fatal("comp_move(): move_to_san() failed\n");
            }
        } else
        {
            if (!move_to_can(move, board, string, 256))
            {
                my_fatal("comp_move(): move_to_can() failed\n");
            }
        }

        xboard_send(XBoard, "move %s", string);

        /**
         * Demitir-se ?
         */

        if (option_get_bool("Resign") && Uci->root_move_nb > 1)
        {
            if (Uci->best_score <= -abs(option_get_int("ResignScore")))
            {
                State->resign_nb++;
                my_log("POLYGLOT %d move%s with resign score\n", State->resign_nb, (State->resign_nb>1) ? "s" : "");

                if (State->resign_nb >= option_get_int("ResignMoves"))
                {
                    my_log("POLYGLOT *** RESIGN ***\n");
                    xboard_send(XBoard, "resign");
                }
            } else
            {
                if (State->resign_nb > 0)
                {
                    my_log("POLYGLOT resign reset (State->resign_nb=%d)\n", State->resign_nb);
                }

                State->resign_nb = 0;
            }
        }

        /**
         * Jogar o movimento.
         */
        move_step(move);
        no_mess(move);
    }

    /**
     *
     */
    static void move_step(int move)
    {
        board_t board[1];
        char move_string[256];

        ASSERT(move_is_ok(move));

        /**
         * Log.
         */
        game_get_board(Game, board);

        if (move != MoveNone && move_is_legal(move, board))
        {
            move_to_san(move, board, move_string, 256);
            my_log("POLYGLOT MOVE %s\n", move_string);
        } else
        {
            move_to_can(move, board, move_string, 256);
            my_log("POLYGLOT ILLEGAL MOVE \"%s\"\n", move_string);
            board_disp(board);

            my_fatal("move_step(): illegal move \"%s\"\n", move_string);
        }

        /**
         * Jogar o movimento.
         */

        game_add_move(Game, move);
        board_update();
    }

    /**
     *
     */
    static void board_update()
    {
        /**
         * Lidar com o final do jogo.
         */
        ASSERT(!XB->result);

        switch (game_status(Game))
        {
            case PLAYING:
                break;

            case WHITE_MATES:
                xboard_send(XBoard, "1-0 {White mates}");
                break;

            case BLACK_MATES:
                xboard_send(XBoard, "0-1 {Black mates}");
                break;

            case STALEMATE:
                xboard_send(XBoard, "1/2-1/2 {Stalemate}");
                break;

            case DRAW_MATERIAL:
                xboard_send(XBoard, "1/2-1/2 {Draw by insufficient material}");
                break;

            case DRAW_FIFTY:
                xboard_send(XBoard, "1/2-1/2 {Draw by fifty-move rule}");
                break;

            case DRAW_REPETITION:
                xboard_send(XBoard, "1/2-1/2 {Draw by repetition}");
                break;

            default:
                ASSERT(false);
                break;
        }
    }

    /**
     *
     */
    static void mess()
    {
        /**
         * Limpar variáveis de estado.
         */
        State->resign_nb = 0;
        State->exp_move = MoveNone;
        my_timer_reset(State->timer);

        /**
         * Terminar uma possível pesquisa.
         */
        stop_search();

        /**
         * Calcular o novo estado.
         */
        if (false)
        {
        } else if (!active())
        {
            State->state = WAIT;
            my_log("POLYGLOT WAIT\n");
        } else if (XB->analyse)
        {
            State->state = ANALYSE;
            my_log("POLYGLOT ANALYSE\n");
        } else if (State->computer[game_turn(Game)])
        {
            State->state = THINK;
            my_log("POLYGLOT THINK\n");
        } else
        {
            State->state = WAIT;
            my_log("POLYGLOT WAIT\n");
        }

        search_update();
    }

    /**
     *
     */
    static void no_mess(int move)
    {
        ASSERT(move_is_ok(move));

        /**
         * Acabou de receber um movimento, calcule o novo estado.
         */
        if (false)
        {
        } else if (!active())
        {
            /**
             * Terminar uma possível pesquisa.
             */
            stop_search();

            State->state = WAIT;
            State->exp_move = MoveNone;
            my_log("POLYGLOT WAIT\n");
        } else if (State->state == WAIT)
        {
            ASSERT(State->computer[game_turn(Game)]);
            ASSERT(!State->computer[colour_opp(game_turn(Game))]);
            ASSERT(!XB->analyse);
            my_log("POLYGLOT WAIT -> THINK\n");

            State->state = THINK;
            State->exp_move = MoveNone;
        } else if (State->state == THINK)
        {
            ASSERT(!State->computer[game_turn(Game)]);
            ASSERT(State->computer[colour_opp(game_turn(Game))]);
            ASSERT(!XB->analyse);

            if (ponder() && ponder_move_is_ok(Uci->ponder_move))
            {
                my_log("POLYGLOT THINK -> PONDER\n");

                State->state = PONDER;
                State->exp_move = Uci->ponder_move;
            } else
            {
                my_log("POLYGLOT THINK -> WAIT\n");

                State->state = WAIT;
                State->exp_move = MoveNone;
            }
        } else if (State->state == PONDER)
        {
            ASSERT(State->computer[game_turn(Game)]);
            ASSERT(!State->computer[colour_opp(game_turn(Game))]);
            ASSERT(!XB->analyse);

            if (move == State->exp_move && Uci->searching)
            {
                ASSERT(Uci->searching);
                ASSERT(Uci->pending_nb >= 1);

                my_timer_reset(State->timer);
                my_timer_start(State->timer);

                my_log("POLYGLOT PONDER -> THINK (*** HIT ***)\n");
                engine_send(Engine,"ponderhit");

                State->state = THINK;
                State->exp_move = MoveNone;

                /**
                 * Exibição de atualização.
                 */
                send_pv();

                /**
                 * Não inicie uma nova pesquisa.
                 */
                return;
            } else
            {
                my_log("POLYGLOT PONDER -> THINK (miss)\n");
                stop_search();

                State->state = THINK;
                State->exp_move = MoveNone;
            }
        } else if (State->state == ANALYSE)
        {
            ASSERT(XB->analyse);

            my_log("POLYGLOT ANALYSE -> ANALYSE\n");
            stop_search();
        } else
        {
            ASSERT(false);
        }

        search_update();
    }

    /**
     *
     */
    static void search_update()
    {
        int move;
        int move_nb;
        board_t board[1];
        ASSERT(!Uci->searching);

        /**
         * Iniciar uma nova pesquisa, se necessário.
         */
        if (State->state == THINK || State->state == PONDER || State->state == ANALYSE)
        {
            /**
             * Abrindo livro.
             */
            if (State->state == THINK && option_get_bool("Book"))
            {
                game_get_board(Game, Uci->board);
                move = book_move(Uci->board, option_get_bool("BookRandom"), option_get_bool("BookWorst"));

                if (move != MoveNone && move_is_legal(move, Uci->board))
                {
                    my_log("POLYGLOT *BOOK MOVE*\n");

                    /**
                     * Limpar Uci->ponder_move.
                     */
                    search_clear();

                    Uci->best_move = move;
                    board_copy(board,Uci->board);
                    move_do(board,move);

                    /**
                     * Movimento esperado = melhor movimento do livro.
                     */
                    Uci->ponder_move = book_move(board, false);
                    Uci->best_pv[0] = Uci->best_move;

                    /**
                     * Pode ser MoveNone.
                     */
                    Uci->best_pv[1] = Uci->ponder_move;
                    Uci->best_pv[2] = MoveNone;
                    comp_move(Uci->best_move);

                    return;
                }
            }

            /**
             * Buscador...
             */
            my_log("POLYGLOT START SEARCH\n");

            /**
             * Opções.
             */
            uci_send_option(Uci, "UCI_Chess960", "%s", option_get_bool("Chess960") ? "true" : "false");

            if (option_get_int("UCIVersion") >= 2)
            {
                uci_send_option(Uci, "UCI_Opponent", "none none %s %s", (XB->computer) ? "computer" : "human", XB->name);
                uci_send_option(Uci, "UCI_AnalyseMode", "%s", (XB->analyse) ? "true" : "false");
            }

            uci_send_option(Uci,"Ponder","%s",ponder()?"true":"false");

            /**
             * Posição...
             */
            move = (State->state == PONDER) ? State->exp_move : MoveNone;

            /**
             * Atualiza a variável global Uci->board.
             */
            send_board(move);

            /**
             * Buscar.
             */

            if (State->state == THINK || State->state == PONDER)
            {
                engine_send_queue(Engine, "go");

                if (XB->time_limit)
                {
                    /**
                     * Tempo fixo por movimento.
                     */
                    engine_send_queue(Engine, " movetime %.0f", XB->time_max * 1000.0);
                } else
                {
                    /**
                     * Controles de tempo.
                     */

                    if (colour_is_white(Uci->board->turn))
                    {
                        engine_send_queue(Engine, " wtime %.0f btime %.0f", XB->my_time * 1000.0, XB->opp_time * 1000.0);
                    } else
                    {
                        engine_send_queue(Engine, " wtime %.0f btime %.0f", XB->opp_time * 1000.0, XB->my_time * 1000.0);
                    }

                    if (XB->inc != 0.0)
                    {
                        engine_send_queue(Engine, " winc %.0f binc %.0f", XB->inc * 1000.0, XB->inc * 1000.0);
                    }

                    if (XB->mps != 0)
                    {
                        move_nb = XB->mps - (Uci->board->move_nb % XB->mps);
                        ASSERT(move_nb >= 1 && move_nb <= XB->mps);

                        engine_send_queue(Engine," movestogo %d",move_nb);
                    }
                }

                if (XB->depth_limit)
                {
                    engine_send_queue(Engine, " depth %d", XB->depth_max);
                }

                if (State->state == PONDER)
                {
                    engine_send_queue(Engine, " ponder");
                }

                /**
                 * Nova linha.
                 */
                engine_send(Engine, "");
            } else if (State->state == ANALYSE)
            {
                engine_send(Engine, "go infinite");
            } else
            {
                ASSERT(false);
            }

            /**
             * Iniciar informações de busca.
             */
            ASSERT(!Uci->searching);
            search_clear();

            Uci->searching = true;
            Uci->pending_nb++;
        }
    }

    /**
     *
     */
    static void search_clear()
    {
        uci_clear(Uci);

        /**
         * Vem me buscar.
         */
        my_timer_reset(State->timer);
        my_timer_start(State->timer);
    }

    /**
     *
     */
    static bool active()
    {
        /**
         * Status da posição.
         */

        if (game_status(Game) != PLAYING)
        {
            /**
             * XXX - Fim de jogo.
             *
             * Observação:
             *     FIXME: O (cú|ãnus) coça no dia seguinte ?
             */
            return false;
        }

        /**
         * Status do xboard.
         */

        if (XB->analyse)
        {
            /**
             * Analisando.
             */
            return true;
        }

        if (!State->computer[White] && !State->computer[Black])
        {
            /**
             * Modo de força ?
             */
            return false;
        }

        if (XB->new_hack || XB->result)
        {
            /**
             * Jogo não iniciado ou finalizado.
             */
            return false;
        }

        /**
         * Jogando.
         */
        return true;
    }

    /**
     *
     */
    static bool ponder()
    {
        return XB->ponder && (option_get_bool("CanPonder") || uci_option_exist(Uci, "Ponder"));
    }

    /**
     *
     */
    static bool ponder_move_is_ok(int move)
    {
        int status;
        board_t board[1];

        ASSERT(move == MoveNone || move_is_ok(move));

        /**
         * Movimento ponderado legal ?
         */
        if (move == MoveNone)
        {
            return false;
        }

        game_get_board(Game,board);

        if (!move_is_legal(move, board))
        {
            return false;
        }

        /**
         * BKS-legal posição resultante ?
         */

        game_add_move(Game, move);
        game_get_board(Game, board);
        status = game_status(Game);

        game_rem_move(Game);

        if (status != PLAYING)
        {
            /**
             * XXX - Fim de jogo.
             *
             * Observação:
             *     FIXME: O (cú|ãnus) coça no dia seguinte ?
             */
            return false;
        }

        if (option_get_bool("Book") && is_in_book(board))
        {
            return false;
        }

        return true;
    }

    /**
     *
     */
    static void stop_search()
    {
        if (Uci->searching)
        {
            ASSERT(Uci->searching);
            ASSERT(Uci->pending_nb >= 1);

            my_log("POLYGLOT STOP SEARCH\n");

            /**
             * engine_send(Engine,"stop");
             * Uci->searching = false;
             */

            if (option_get_bool("SyncStop"))
            {
                uci_send_stop_sync(Uci);
            } else
            {
                uci_send_stop(Uci);
            }
        }
    }

    /**
     * //
     * // Sair ? rsrs.
     * //
     * static void quit()
     * {
     *     char string[StringSize];
     *     my_log("POLYGLOT *** QUIT ***\n");
     *
     *     stop_search();
     *     engine_send(Engine,"quit");
     *
     *     //
     *     // Espere a especificação parar.
     *     //
     *
     *     while (true)
     *     {
     *         //
     *         // Chamar exit() ao receber EOF.
     *         //
     *         engine_get(Engine, string, StringSize);
     *     }
     *
     *     uci_close(Uci);
     *     exit(EXIT_SUCCESS);
     * }
     */

    /**
     *
     */
    static void send_board(int extra_move)
    {
        char fen[256];
        int start, end;
        board_t board[1];

        int pos;
        int move;
        char string[256];

        ASSERT(extra_move == MoveNone || move_is_ok(extra_move));
        ASSERT(!Uci->searching);

        /**
         * Iniciar.
         */
        game_get_board(Game, Uci->board);

        if (extra_move != MoveNone)
        {
            move_do(Uci->board, extra_move);
        }

        board_to_fen(Uci->board, fen, 256);
        my_log("POLYGLOT FEN %s\n", fen);
        ASSERT(board_can_play(Uci->board));

        /**
         * Mais inicialização.
         */
        start = 0;
        end = game_pos(Game);
        ASSERT(end >= start);

        /**
         * Qual a posição correta ?
         */
        game_get_board(Game, board, start);
        board_to_fen(board, string, 256);
        engine_send_queue(Engine, "position");

        if (my_string_equal(string, StartFen))
        {
            engine_send_queue(Engine, " startpos");
        } else
        {
            engine_send_queue(Engine, " fen %s", string);
        }

        /**
         * Mover lista.
         */
        if (end > start || extra_move != MoveNone)
        {
            engine_send_queue(Engine, " moves");
        }

        for (pos = start; pos < end; pos++)
        {
            /**
             * Movimentos do jogo.
             */
            move = game_move(Game, pos);

            move_to_can(move, board, string, 256);
            engine_send_queue(Engine, " %s", string);
            move_do(board, move);
        }

        if (extra_move != MoveNone)
        {
            /**
             * Mover para refletir sobre.
             */
            move_to_can(extra_move, board, string, 256);
            engine_send_queue(Engine, " %s", string);
        }

        /**
         * Dançar ?
         */

        /**
         * Nova linha.
         */
        engine_send(Engine, "");
    }

    /**
     *
     */
    static void send_pv()
    {
        char pv_string[StringSize];
        board_t board[1];

        int move;
        char move_string[StringSize];

        ASSERT(State->state != WAIT);

        if (Uci->best_depth == 0)
        {
            return;
        }

        /**
         * Informações de pesquisa xboard.
         */
        if (XB->post)
        {
            if (State->state == THINK || State->state == ANALYSE)
            {
                line_to_san(Uci->best_pv, Uci->board, pv_string, StringSize);
                xboard_send(XBoard, "%d %+d %.0f %lld %s", Uci->best_depth, Uci->best_score, Uci->time * 100.0, Uci->node_nb, pv_string);
            } else if (State->state == PONDER && option_get_bool("ShowPonder"))
            {
                game_get_board(Game, board);
                move = State->exp_move;

                if (move != MoveNone && move_is_legal(move, board))
                {
                    move_to_san(move, board, move_string, 256);
                    line_to_san(Uci->best_pv, Uci->board, pv_string, StringSize);
                    xboard_send(XBoard, "%d %+d %.0f %lld (%s) %s", Uci->best_depth, Uci->best_score, Uci->time * 100.0, Uci->node_nb, move_string, pv_string);
                }
            }
        }

        /**
         * kibitz.
         */
        if ((Uci->searching && option_get_bool("KibitzPV") && Uci->time >= option_get_double("KibitzDelay")) || (!Uci->searching && option_get_bool("KibitzMove")))
        {
            if (State->state == THINK || State->state == ANALYSE)
            {
                line_to_san(Uci->best_pv, Uci->board, pv_string, StringSize);
                xboard_send(XBoard, "%s depth=%d time=%.2f node=%lld speed=%.0f score=%+.2f pv=\"%s\"", option_get_string("KibitzCommand"), Uci->best_depth, Uci->time, Uci->node_nb, Uci->speed, double(Uci->best_score) / 100.0, pv_string);
            } else if (State->state == PONDER)
            {
                game_get_board(Game, board);
                move = State->exp_move;

                if (move != MoveNone && move_is_legal(move, board))
                {
                    move_to_san(move, board, move_string, 256);
                    line_to_san(Uci->best_pv, Uci->board, pv_string, StringSize);
                    xboard_send(XBoard, "%s depth=%d time=%.2f node=%lld speed=%.0f score=%+.2f pv=\"(%s) %s\"", option_get_string("KibitzCommand"), Uci->best_depth, Uci->time, Uci->node_nb, Uci->speed, double(Uci->best_score) / 100.0, move_string, pv_string);
                }
            }
        }
    }

    /**
     *
     */
    static void xboard_get(xboard_t * xboard, char string[], int size)
    {
        ASSERT(xboard != NULL);
        ASSERT(string != NULL);
        ASSERT(size >= 256);

        if (!io_get_line(xboard->io, string, size))
        {
            /**
             * EOF.
             */
            my_log("POLYGLOT *** EOF from XBoard ***\n");
            quit();
        }
    }

    /**
     *
     */
    static void xboard_send(xboard_t * xboard, const char format[], ...)
    {
        va_list arg_list;
        char string[StringSize];

        ASSERT(xboard != NULL);
        ASSERT(format != NULL);

        /**
         * Formato.
         */
        va_start(arg_list, format);
        vsprintf(string, format, arg_list);
        va_end(arg_list);

        /**
         * Acompanhar.
         */
        io_send(xboard->io, "%s", string);
    }

    /**
     * Apreender.
     */
    static void learn(int result)
    {
        int pos;
        board_t board[1];
        int move;

        ASSERT(result >= -1 && result <= +1);
        ASSERT(XB->result);
        ASSERT(State->computer[White] || State->computer[Black]);

        /**
         * Iniciar.
         */

        /**
         * A posição inicial.
         */
        pos = 0;

        if (false)
        {
        } else if (State->computer[White])
        {
            pos = 0;
        } else if (State->computer[Black])
        {
            /**
             * A posição do cavalo.
             */
            pos = 1;

            result = -result;
        } else
        {
            my_fatal("learn(): unknown side\n");
        }

        if (false)
        {
        } else if (result > 0)
        {
            my_log("POLYGLOT *LEARN WIN*\n");
        } else if (result < 0)
        {
            my_log("POLYGLOT *LEARN LOSS*\n");
        } else
        {
            my_log("POLYGLOT *LEARN DRAW*\n");
        }

        /**
         * Enquanto...
         *
         * Português:
         *     Enquanto a posição for menor que do tamanho do jogador,
         *     inserir mais dois ná posição...
         */
        for (; pos < Game->size; pos += 2)
        {
            game_get_board(Game, board, pos);
            move = game_move(Game, pos);

            book_learn_move(board, move, result);
        }

        book_flush();
    }
}
