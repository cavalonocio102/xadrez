/**
 * pgn.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef PGN_H
#define PGN_H

    #include <cstdio>
    #include "util.h"

    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int PGN_STRING_SIZE = 256;

        /**
         * Tipos.
         */

        /**
         *
         */
        struct pgn_t
        {
            /**
             *
             */
            FILE * file;

            /**
             *
             */
            int char_hack;

            /**
             *
             */
            int char_line;

            /**
             *
             */
            int char_column;

            /**
             *
             */
            bool char_unread;

            /**
             *
             */
            bool char_first;

            /**
             *
             */
            int token_type;

            /**
             *
             */
            char token_string[PGN_STRING_SIZE];

            /**
             *
             */
            int token_length;

            /**
             *
             */
            int token_line;

            /**
             *
             */
            int token_column;

            /**
             *
             */
            bool token_unread;

            /**
             *
             */
            bool token_first;

            /**
             *
             */
            char result[PGN_STRING_SIZE];

            /**
             *
             */
            char fen[PGN_STRING_SIZE];

            /**
             *
             */
            int move_line;

            /**
             *
             */
            int move_column;
        };

        /**
         * Funções.
         */

        /**
         *
         */
        extern void pgn_open(pgn_t * pgn, const char file_name[]);

        /**
         *
         */
        extern void pgn_close(pgn_t * pgn);

        /**
         *
         */
        extern bool pgn_next_game(pgn_t * pgn);

        /**
         *
         */
        extern bool pgn_next_move(pgn_t * pgn, char string[], int size);
    }

#endif
