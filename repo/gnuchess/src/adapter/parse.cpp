/**
 * parse.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */

#include <cstring>

#include "parse.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Constantes.
     */

    /**
     *
     */
    static const int StringSize = 256;

    /**
     * Variáveis.
     */

    /**
     *
     */
    char * Star[STAR_NUMBER];

    /**
     * Protótipos.
     */

    /**
     *
     */
    static bool match_rec(char string[], const char pattern[], char * star[]);

    /**
     * Funções.
     */

    /**
     *
     */
    bool match(char string[], const char pattern[])
    {
        ASSERT(string != NULL);
        ASSERT(pattern != NULL);
        ASSERT(strstr(pattern, "**") == NULL);

        return match_rec(string, pattern, Star);
    }

    /**
     *
     */
    static bool match_rec(char string[], const char pattern[], char * star[])
    {
        int c;

        ASSERT(string != NULL);
        ASSERT(pattern != NULL);
        ASSERT(star != NULL);

        /**
         * Correspondências iterativas.
         */

        while ((c = *pattern++) != '*')
        {
            if (false)
            {
            } else if (c == '\0')
            {
                /**
                 * Fim do padrão.
                 */
                while (*string == ' ')
                {
                    /**
                     * Pular espaços à direita.
                     */
                    string++;
                }

                return *string == '\0';
            } else if (c == ' ')
            {
                /**
                 * Espaços em branco.
                 */
                if (*string++ != ' ')
                {
                    /**
                     * Incompatibilidade.
                     */
                    return false;
                }

                while (*string == ' ')
                {
                    /**
                     * Pular espaços à direita.
                     */
                    string++;
                }
            } else
            {
                /**
                 * Personagem normal.
                 */
                if (*string++ != c)
                {
                    /**
                     * Incompatibilidade.
                     */
                    return false;
                }
            }
        }

        /**
         * Correspondência de observações recursiva.
         */
        ASSERT(c == '*');

        while (*string == ' ')
        {
            /**
             * Pular espaços iniciais.
             */
            string++;
        }

        /**
         * Lembre-se do começo da peça estrela.
         */
        *star++ = string;

        while ((c = *string++) != '\0')
        {
            /**
             * Rejeitar correspondência de string vazia.
             */
            if (c != ' ' && match_rec(string, pattern, star))
            {
                /**
                 * Inicialização mais curta.
                 */
                ASSERT(string > star[-1]);

                /**
                 * Peça estrela truncada.
                 */
                *string = '\0';

                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    bool parse_is_ok(const parse_t * parse)
    {
        if (parse == NULL)
        {
            return false;
        }

        if (parse->string == NULL)
        {
            return false;
        }

        if (parse->pos < 0 || parse->pos > strlen(parse->string))
        {
            return false;
        }

        if (parse->keyword_nb < 0 || parse->keyword_nb >= KEYWORD_NUMBER)
        {
            return false;
        }

        return true;
    }

    /**
     *
     */
    void parse_open(parse_t * parse, const char string[])
    {
        ASSERT(parse != NULL);
        ASSERT(string != NULL);

        parse->string = string;
        parse->pos = 0;
        parse->keyword_nb = 0;
    }

    /**
     *
     */
    void parse_close(parse_t * parse)
    {
        int i;

        ASSERT(parse_is_ok(parse));
        parse->string = NULL;
        parse->pos = 0;

        for (i = 0; i < parse->keyword_nb; i++)
        {
            my_string_clear(&parse->keyword[i]);
        }

        parse->keyword_nb = 0;
    }

    /**
     *
     */
    void parse_add_keyword(parse_t * parse, const char keyword[])
    {
        const char * * string;

        ASSERT(parse_is_ok(parse));
        ASSERT(keyword != NULL);

        if (parse->keyword_nb < KEYWORD_NUMBER)
        {
            string = &parse->keyword[parse->keyword_nb];
            parse->keyword_nb++;

            *string = NULL;
            my_string_set(string, keyword);
        }
    }

    /**
     *
     */
    bool parse_get_word(parse_t * parse, char string[], int size)
    {
        int pos;
        int c;

        ASSERT(parse != NULL);
        ASSERT(string != NULL);
        ASSERT(size >= 256);

        /**
         * Pular espaços em branco.
         */
        for (; parse->string[parse->pos] == ' '; parse->pos++)
        {
        }

        ASSERT(parse->string[parse->pos] != ' ');

        /**
         * Copiar palavra.
         */
        pos = 0;

        while (true)
        {
            c = parse->string[parse->pos];

            if (c == ' ' || pos >= size - 1)
            {
                c = '\0';
            }

            string[pos] = c;
            if (c == '\0')
            {
                break;
            }

            parse->pos++;
            pos++;
        }

        ASSERT(strchr(string, ' ') == NULL);

        /**
         * Palavra não vazia ?
         */
        return pos > 0;
    }

    /**
     *
     */
    bool parse_get_string(parse_t * parse, char string[], int size)
    {
        int pos;
        parse_t parse_2[1];

        char word[StringSize];
        int i;
        int c;

        ASSERT(parse != NULL);
        ASSERT(string != NULL);
        ASSERT(size >= 256);

        /**
         * Pular espaços em branco.
         */
        for (; parse->string[parse->pos] == ' '; parse->pos++)
        {
        }

        ASSERT(parse->string[parse->pos] != ' ');

        /**
         * Copiar string.
         */
        pos = 0;

        while (true)
        {
            parse_open(parse_2, &parse->string[parse->pos]);

            if (!parse_get_word(parse_2, word, StringSize))
            {
                string[pos] = '\0';
                parse_close(parse_2);
                goto finished;
            }

            for (i = 0; i < parse->keyword_nb; i++)
            {
                if (my_string_equal(parse->keyword[i], word))
                {
                    string[pos] = '\0';
                    parse_close(parse_2);
                    goto finished;
                }
            }

            parse_close(parse_2);

            /**
             * Copie os espaços.
             */
            while (true)
            {
                c = parse->string[parse->pos];

                if (c != ' ')
                {
                    break;
                }

                if (pos >= size - 1)
                {
                    c = '\0';
                }

                string[pos] = c;
                if (c == '\0')
                {
                    break;
                }

                parse->pos++;
                pos++;
            }

            /**
             * Copiar não espaços.
             */
            while (true)
            {
                c = parse->string[parse->pos];
                if (c == ' ' || pos >= size - 1)
                {
                    c = '\0';
                }

                string[pos] = c;

                if (c == '\0')
                {
                    break;
                }

                parse->pos++;
                pos++;
            }

            string[pos] = '\0';
        }

        finished: ; // Questão: Parou o GoTo aqui ?

        /**
         * string não vazia ?
         */
        return pos > 0;
    }
}
