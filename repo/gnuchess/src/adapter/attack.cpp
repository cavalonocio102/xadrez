/**
 * attack.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */

#include "board.h"
#include "colour.h"
#include "move.h"
#include "attack.h"
#include "piece.h"
#include "util.h"


/**
 * Macros.
 */

/**
 * Fixme: ?
 */
#define DELTA_INC(delta) (DeltaInc[128 + (delta)])

/**
 * Fixme: ?
 */
#define DELTA_MASK(delta) (DeltaMask[128 + (delta)])


/**
 *
 */
namespace adapter
{
    /**
     * "Constantes".
     */

    /**
     *
     */
    const sint8 KnightInc[8 + 1] =
    {
        -33,
        -31,
        -18,
        -14,
        +14,
        +18,
        +31,
        +33,
        0
    };

    /**
     * +------------+
     * |   @        |
     * |  /|\ +_\@  | <= (1 ~> 4) = "Dois Homems Jogando Xadrez".
     * |  \|->| /   |
     * |  /|  |\    |
     * +------------+
     *
     * Questão: São posições ?
     */
    const sint8 BishopInc[4 + 1] =
    {
        -17,
        -15,
        +15,
        +17,
        0
    }; // Foi bom ?

    /**
     * +------------+
     * |   @        |
     * |  /|\ +_\@  | <= (1 ~> 4) = "Dois Homems Jogando Xadrez".
     * |  \|->| /   |
     * |  /|  |\    |
     * +------------+
     *
     * Fixme: São posições ?
     */
    const sint8 RookInc[4 + 1] =
    {
        -16,
        -1,
        +1,
        +16,
        0
    }; // Foi bom ?

    /**
     *
     */
    const sint8 QueenInc[8 + 1] =
    {
        -17,
        -16,
        -15,
         -1,
         +1,
        +15,
        +16,
        +17,
        0
    };

    /**
     *
     */
    const sint8 KingInc[8 + 1] =
    {
        -17,
        -16,
        -15,
         -1,
         +1,
        +15,
        +16,
        +17,
        0
    };

    /**
     * Variáveis.
     */

    /**
     *
     */
    static sint8 DeltaInc[256];

    /**
     *
     */
    static uint8 DeltaMask[256];

    /**
     * Protótipos.
     */

    /**
     *
     */
    static bool delta_is_ok(int delta);

    /**
     *
     */
    static bool inc_is_ok(int inc);

    /**
     * Funções.
     */

    /**
     * Iniciar um novo ataque.
     */
    void attack_init()
    {
        int delta;
        int dir, inc, dist;

        for (delta = -128; delta < +128; delta++)
        {
            DeltaInc[128 + delta] = IncNone;
            DeltaMask[128 + delta] = 0;
        }

        DeltaMask[128 - 17] |= BlackPawnFlag;
        DeltaMask[128 - 15] |= BlackPawnFlag;

        DeltaMask[128+15] |= WhitePawnFlag;
        DeltaMask[128+17] |= WhitePawnFlag;

        for (dir = 0; dir < 8; dir++)
        {
            delta = KnightInc[dir];
            ASSERT(delta_is_ok(delta));
            DeltaMask[128 + delta] |= KnightFlag;
        }

        /**
         * +------------+
         * |   @        |
         * |  /|\ +_\@  | <= (1 ~> 4) = "Dois Homems Jogando Xadrez".
         * |  \|->| /   |
         * |  /|  |\    |
         * +------------+
         *
         * Fixme: (dir 4) = posição de quatro ?
         */
        for (dir = 0; dir < 4; dir++)
        {
            /**
             * Voltas ?
             */

            inc = BishopInc[dir];
            ASSERT(inc != IncNone);

            for (dist = 1; dist < 8; dist++)
            {
                delta = inc*dist;
                ASSERT(delta_is_ok(delta));
                ASSERT(DeltaInc[128 + delta] == IncNone);

                DeltaInc[128 + delta] = inc;
                DeltaMask[128 + delta] |= BishopFlag;
            }
        }

        /**
         * +------------+
         * |   @        |
         * |  /|\ +_\@  | <= (1 ~> 4) = "Dois Homems Jogando Xadrez".
         * |  \|->| /   |
         * |  /|  |\    |
         * +------------+
         *
         * Fixme: (dir 4) = posição de quatro ?
         */
        for (dir = 0; dir < 4; dir++)
        {
            /**
             * Voltas ?
             */

            inc = RookInc[dir];
            ASSERT(inc!=IncNone);

            for (dist = 1; dist < 8; dist++)
            {
                delta = inc*dist;
                ASSERT(delta_is_ok(delta));
                ASSERT(DeltaInc[128 + delta] == IncNone);

                DeltaInc[128 + delta] = inc;
                DeltaMask[128 + delta] |= RookFlag;
            }
        }

        for (dir = 0; dir < 8; dir++)
        {
            delta = KingInc[dir];

            ASSERT(delta_is_ok(delta));
            DeltaMask[128 + delta] |= KingFlag;
        }
    }

    /**
     *
     */
    static bool delta_is_ok(int delta)
    {
        if (delta < -119 || delta > +119)
        {
            return false;
        }

        return true;
    }

    /**
     *
     */
    static bool inc_is_ok(int inc)
    {
        int dir;

        for (dir = 0; dir < 8; dir++)
        {
            if (KingInc[dir] == inc)
            {
                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    bool is_in_check(const board_t * board, int colour)
    {
        ASSERT(board_is_ok(board));
        ASSERT(colour_is_ok(colour));

        return is_attacked(board, king_pos(board, colour), colour_opp(colour));
    }

    /**
     *
     */
    bool is_attacked(const board_t * board, int to, int colour)
    {
        const uint8 * ptr;
        int from, piece;

        ASSERT(board_is_ok(board));
        ASSERT(square_is_ok(to));
        ASSERT(colour_is_ok(colour));

        for (ptr = board->list[colour]; (from = *ptr) != SquareNone; ptr++)
        {
            piece = board->square[from];
            ASSERT(colour_equal(piece, colour));

            if (piece_attack(board, piece, from, to))
            {
                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    bool piece_attack(const board_t * board, int piece, int from, int to)
    {
        int delta;
        int inc, sq;

        ASSERT(board_is_ok(board));
        ASSERT(piece_is_ok(piece));
        ASSERT(square_is_ok(from));
        ASSERT(square_is_ok(to));

        delta = to - from;
        ASSERT(delta_is_ok(delta));

        if ((piece & DELTA_MASK(delta)) == 0)
        {
            /**
             * Sem pseudo-ataque, por enquanto.
             */
            return false;
        }

        if (!piece_is_slider(piece))
        {
            return true;
        }

        inc = DELTA_INC(delta);
        ASSERT(inc_is_ok(inc));

        for (sq = from + inc; sq != to; sq += inc)
        {
            ASSERT(square_is_ok(sq));

            if (board->square[sq] != Empty) return false; // blocker
        }

        return true;
    }

    /**
     *
     */
    bool is_pinned(const board_t * board, int from, int to, int colour)
    {
        int king;
        int inc;
        int sq, piece;

        ASSERT(board != NULL);
        ASSERT(square_is_ok(from));
        ASSERT(square_is_ok(to));
        ASSERT(colour_is_ok(colour));

        king = king_pos(board, colour);
        inc = DELTA_INC(king - from);

        if (inc == IncNone)
        {
            /**
             * Não é uma linha.
             */
            return false;
        }

        sq = from;

        do
        {
            sq += inc;
        } while (board->square[sq] == Empty);

        if (sq != king)
        {
            /**
             * Bloqueador.
             */
            return false;
        }

        sq = from;

        do
        {
            sq -= inc;
        } while ((piece = board->square[sq]) == Empty);

        return square_is_ok(sq)
            && (piece & DELTA_MASK(king - sq)) != 0
            && piece_colour(piece) == colour_opp(colour)
            && DELTA_INC(king-to) != inc;
    }
}
