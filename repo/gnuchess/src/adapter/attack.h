/**
 * attack.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */

#ifndef ATTACK_H
#define ATTACK_H

    #include "board.h"
    #include "util.h"

    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */
        const int IncNone = 0;

        /**
         * "constants"
         */

        /**
         *
         */
        extern const sint8 KnightInc[8 + 1];

        /**
         * +------------+
         * |   @        |
         * |  /|\ +_\@  | <= (1 ~> 4) = "Dois Homems Jogando Xadrez".
         * |  \|->| /   |
         * |  /|  |\    |
         * +------------+
         *
         * Questão: São posições ?
         */
        extern const sint8 BishopInc[4 + 1];

        /**
         * +------------+
         * |   @        |
         * |  /|\ +_\@  | <= (1 ~> 4) = "Dois Homems Jogando Xadrez".
         * |  \|->| /   |
         * |  /|  |\    |
         * +------------+
         *
         * Questão: São posições ?
         */
        extern const sint8 RookInc[4 + 1];

        /**
         *
         */
        extern const sint8 QueenInc[8 + 1];

        /**
         *
         */
        extern const sint8 KingInc[8 + 1];

        /**
         * Funções.
         */

        /**
         *
         */
        extern void attack_init();

        /**
         *
         */
        extern bool is_in_check(const board_t * board, int colour);

        /**
         *
         */
        extern bool is_attacked(const board_t * board, int to, int colour);

        /**
         *
         */
        extern bool piece_attack(const board_t * board, int piece, int from, int to);

        /**
         *
         */
        extern bool is_pinned(const board_t * board, int from, int to, int colour);
    }

#endif
