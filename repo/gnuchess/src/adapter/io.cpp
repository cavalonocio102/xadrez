/**
 * io.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include <cerrno>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>

#include <sys/types.h>
#include <unistd.h>

#include "io.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Constantes.
     */

    /**
     *
     */
    static const bool UseDebug = false;

    /**
     * True no Cavalas.
     */
    static const bool UseCR = false;

    /**
     *
     */
    static const int StringSize = 4096;

    /**
     *
     */
    static const char LF = '\n';

    /**
     *
     */
    static const char CR = '\r';

    /**
     * Protótipos.
     */

    /**
     *
     */
    static int  my_read(int fd, char string[], int size);

    /**
     *
     */
    static void my_write(int fd, const char string[], int size);

    /**
     * Funções.
     */

    /**
     *
     */
    bool io_is_ok(const io_t * io)
    {
        if (io == NULL)
        {
            return false;
        }

        if (io->name == NULL)
        {
            return false;
        }

        if (io->in_eof != true && io->in_eof != false)
        {
            return false;
        }

        if (io->in_size < 0 || io->in_size > BufferSize)
        {
            return false;
        }

        if (io->out_size < 0 || io->out_size > BufferSize)
        {
            return false;
        }

        return true;
    }

    /**
     *
     */
    void io_init(io_t * io)
    {
        ASSERT(io != NULL);

        io->in_eof = false;
        io->in_size = 0;
        io->out_size = 0;

        ASSERT(io_is_ok(io));
    }

    /**
     *
     */
    void io_close(io_t * io)
    {
        ASSERT(io_is_ok(io));
        ASSERT(io->out_fd >= 0);
        my_log("> %s EOF\n", io->name);

        if (close(io->out_fd) == -1)
        {
            my_fatal("io_close(): close(): %s\n", strerror(errno));
        }

        io->out_fd = -1;
    }

    /**
     *
     */
    void io_get_update(io_t * io)
    {
        int pos, size;
        int n;

        ASSERT(io_is_ok(io));
        ASSERT(io->in_fd >= 0);
        ASSERT(!io->in_eof);

        /**
         * Iniciar.
         */
        pos = io->in_size;
        size = BufferSize - pos;

        if (size <= 0)
        {
            my_fatal("io_get_update(): buffer overflow\n");
        }

        /**
         * Ler o máximo de dados possível.
         */
        n = my_read(io->in_fd, &io->in_buffer[pos], size);

        if (UseDebug)
        {
            my_log("POLYGLOT read %d byte%s from %s\n", n, (n > 1) ? "s" : "", io->name);
        }

        if (n > 0)
        {
            /**
             * Pelo menos um caractere foi lido.
             * Atualizar tamanho do buffer.
             */
            ASSERT(n >= 1 && n <= size);
            io->in_size += n;

            ASSERT(io->in_size >= 0 && io->in_size <= BufferSize);
        } else
        {
            /**
             * EOF.
             */
            ASSERT(n == 0);
            io->in_eof = true;
        }
    }

    /**
     *
     */
    bool io_line_ready(const io_t * io)
    {
        ASSERT(io_is_ok(io));

        if (io->in_eof)
        {
            return true;
        }

        if (memchr(io->in_buffer, LF, io->in_size) != NULL)
        {
            /**
             * Buffer contém LF.
             */
            return true;
        }

        return false;
    }

    /**
     *
     */
    bool io_get_line(io_t * io, char string[], int size)
    {
        int src, dst;
        int c;

        ASSERT(io_is_ok(io));
        ASSERT(string != NULL);
        ASSERT(size >= 256);

        src = 0;
        dst = 0;

        while (true)
        {
            /**
             * Teste para o fim do buffer.
             */
            if (src >= io->in_size)
            {
                if (io->in_eof)
                {
                    my_log("< %s EOF\n",io->name);

                    return false;
                } else
                {
                    my_fatal("io_get_line(): no EOL in buffer\n");
                }
            }

            /**
             * Teste para o fim da string.
             */
            if (dst >= size)
            {
                my_fatal("io_get_line(): buffer overflow\n");
            }

            /**
             * Copie o próximo caractere.
             */
            c = io->in_buffer[src++];

            if (c == LF)
            {
                /**
                 * LF => linha completa.
                 */
                string[dst] = '\0';
                break;
            } else if (c != CR)
            {
                /**
                 * Pular CRs.
                 */
                string[dst++] = c;
            }
        }

        /**
         * Mudar o buffer.
         */
        ASSERT(src > 0);

        io->in_size -= src;
        ASSERT(io->in_size >= 0);

        if (io->in_size > 0)
        {
            memmove(&io->in_buffer[0], &io->in_buffer[src], io->in_size);
        }

        /**
         * Retornar.
         */
        my_log("< %s %s\n", io->name, string);

        return true;
    }

    /**
     *
     */
    void io_send(io_t * io, const char format[], ...)
    {
        va_list arg_list;
        char string[StringSize];
        int len;

        ASSERT(io_is_ok(io));
        ASSERT(format != NULL);
        ASSERT(io->out_fd >= 0);

        /**
         * Formato.
         */
        va_start(arg_list, format);
        vsprintf(string, format, arg_list);
        va_end(arg_list);

        /**
         * Anexar string ao buffer.
         */
        len = strlen(string);
        if (io->out_size + len > BufferSize - 2)
        {
            my_fatal("io_send(): buffer overflow\n");
        }

        memcpy(&io->out_buffer[io->out_size], string, len);
        io->out_size += len;
        ASSERT(io->out_size >= 0 && io->out_size <= BufferSize - 2);

        /**
         * Logs.
         */
        io->out_buffer[io->out_size] = '\0';
        my_log("> %s %s\n", io->name, io->out_buffer);

        /**
         * Anexar EOL ao buffer.
         */
        if (UseCR)
        {
            io->out_buffer[io->out_size++] = CR;
        }

        io->out_buffer[io->out_size++] = LF;
        ASSERT(io->out_size >= 0 && io->out_size <= BufferSize);

        /**
         * Tampão de plug.
         */
        if (UseDebug)
        {
            my_log("POLYGLOT writing %d byte%s to %s\n", io->out_size, (io->out_size > 1) ? "s" : "", io->name);
        }

        my_write(io->out_fd, io->out_buffer, io->out_size);
        io->out_size = 0;
    }

    /**
     *
     */
    void io_send_queue(io_t * io, const char format[], ...)
    {
        va_list arg_list;
        char string[StringSize];
        int len;

        ASSERT(io_is_ok(io));
        ASSERT(format != NULL);
        ASSERT(io->out_fd >= 0);

        /**
         * Formato.
         */
        va_start(arg_list, format);
        vsprintf(string, format, arg_list);
        va_end(arg_list);

        /**
         * Anexar string ao buffer.
         */
        len = strlen(string);
        if (io->out_size + len > BufferSize - 2)
        {
            my_fatal("io_send_queue(): buffer overflow\n");
        }

        memcpy(&io->out_buffer[io->out_size], string, len);
        io->out_size += len;

        ASSERT(io->out_size >= 0 && io->out_size <= BufferSize - 2);
    }

    /**
     *
     */
    static int my_read(int fd, char string[], int size)
    {
        int n;

        ASSERT(fd >= 0);
        ASSERT(string != NULL);
        ASSERT(size > 0);

        do
        {
            n = read(fd, string, size);
        } while (n == -1 && errno == EINTR);

        if (n == -1)
        {
            my_fatal("my_read(): read(): %s\n", strerror(errno));
        }

        ASSERT(n >= 0);

        return n;
    }

    /**
     *
     */
    static void my_write(int fd, const char string[], int size)
    {
        int n;

        ASSERT(fd >= 0);
        ASSERT(string != NULL);
        ASSERT(size > 0);

        do
        {
            n = write(fd, string, size);

            /**
             * if (n == -1 && errno != EINTR && errno != EPIPE) my_fatal("my_write(): write(): %s\n",strerror(errno));
             */

            if (n == -1)
            {
                if (false)
                {
                } else if (errno == EINTR)
                {
                    /**
                     * Nada foi escrito...
                     */
                    n = 0;
                } else if (errno == EPIPE)
                {
                    /**
                     * Fingir que tudo foi escrito.
                     */
                    n = size;
                } else
                {
                    my_fatal("my_write(): write(): %s\n", strerror(errno));
                }
            }

            ASSERT(n >= 0);
            string += n;
            size -= n;
        } while (size > 0);

        ASSERT(size == 0);
    }
}
