/**
 * move_legal.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef MOVE_LEGAL_H
#define MOVE_LEGAL_H

    #include "board.h"
    #include "list.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Funções.
         */

        /**
         *
         */
        extern bool move_is_pseudo(int move, const board_t * board);

        /**
         *
         */
        extern bool pseudo_is_legal(int move, const board_t * board);

        /**
         *
         */
        extern bool move_is_legal(int move, const board_t * board);

        /**
         *
         */
        extern void filter_legal(list_t * list, const board_t * board);
    }

#endif
