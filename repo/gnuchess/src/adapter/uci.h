/**
 * uci.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef UCI_H
#define UCI_H

    #include "board.h"
    #include "engine.h"
    #include "line.h"
    #include "move.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         *
         */
        const int OptionNb = 256;

        /**
         * Tipos.
         */

        /**
         *
         */
        struct option_t
        {
            /**
             *
             */
            const char * name;

            /**
             *
             */
            const char * value;
        };

        /**
         *
         */
        struct uci_t
        {
            /**
             *
             */
            engine_t * engine;

            /**
             *
             */
            const char * name;

            /**
             *
             */
            const char * author;

            /**
             *
             */
            int option_nb;

            /**
             *
             */
            option_t option[OptionNb];

            /**
             *
             */
            bool ready;

            /**
             *
             */
            int ready_nb;

            /**
             *
             */
            bool searching;

            /**
             *
             */
            int pending_nb;

            /**
             *
             */
            board_t board[1];

            /**
             *
             */
            int best_move;

            /**
             *
             */
            int ponder_move;

            /**
             *
             */
            int score;

            /**
             *
             */
            int depth;

            /**
             *
             */
            int sel_depth;

            /**
             *
             */
            move_t pv[LineSize];

            /**
             *
             */
            int best_score;

            /**
             *
             */
            int best_depth;

            /**
             *
             */
            int best_sel_depth;

            /**
             *
             */
            move_t best_pv[LineSize];

            /**
             *
             */
            sint64 node_nb;

            /**
             *
             */
            double time;

            /**
             *
             */
            double speed;

            /**
             *
             */
            double cpu;

            /**
             *
             */
            double hash;

            /**
             *
             */
            move_t current_line[LineSize];

            /**
             *
             */
            int root_move;

            /**
             *
             */
            int root_move_pos;

            /**
             *
             */
            int root_move_nb;
        };

        /**
         *
         */
        enum dummy_event_t
        {
            EVENT_NONE = 0,
            EVENT_UCI = 1 << 0,
            EVENT_READY = 1 << 1,
            EVENT_STOP = 1 << 2,
            EVENT_MOVE = 1 << 3,
            EVENT_PV = 1 << 4,
            EVENT_DEPTH = 1 << 5
        };

        /**
         * Variáveis.
         */

        /**
         *
         */
        extern uci_t Uci[1];

        /**
         * Funções.
         */

        /**
         *
         */
        extern void uci_open(uci_t * uci, engine_t * engine);

        /**
         *
         */
        extern void uci_close(uci_t * uci);

        /**
         *
         */
        extern void uci_clear(uci_t * uci);

        /**
         *
         */
        extern void uci_send_isready(uci_t * uci);

        /**
         *
         */
        extern void uci_send_isready_sync(uci_t * uci);

        /**
         *
         */
        extern void uci_send_stop(uci_t * uci);

        /**
         *
         */
        extern void uci_send_stop_sync(uci_t * uci);

        /**
         *
         */
        extern void uci_send_ucinewgame(uci_t * uci);

        /**
         *
         */
        extern bool uci_option_exist(uci_t * uci, const char option[]);

        /**
         *
         */
        extern void uci_send_option(uci_t * uci, const char option[], const char format[], ...);

        /**
         *
         */
        extern int  uci_parse(uci_t * uci, const char string[]);
    }

#endif
