/**
 * util.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include <cctype>
#include <cerrno>
#include <cmath>
#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>

#include "main.h"
#include "posix.h"
#include "util.h"
#include "configmake.h"


/**
 *
 */
namespace adapter
{
    /**
     * Variáveis constantes.
     */

    /**
     *
     */
    const int MaxFileNameSize = 256;

    /**
     * Variáveis.
     */

    /**
     *
     */
    static bool Error;

    /**
     *
     */
    static FILE * LogFile;

    /**
     * Funções.
     */

    /**
     *
     */
    void util_init()
    {
        Error = false;

        /**
         * Arquivo de log de inicialização.
         */
        LogFile = NULL;
    }

    /**
     *
     */
    void my_random_init()
    {
        srand(time(NULL));
    }

    /**
     *
     */
    int my_random_int(int n)
    {
        int r;

        ASSERT(n > 0);

        r = int(floor(my_random_double()*double(n)));
        ASSERT(r >= 0 && r < n);

        return r;
    }

    /**
     *
     */
    double my_random_double()
    {
        double r;

        r = double(rand()) / (double(RAND_MAX) + 1.0);
        ASSERT(r >= 0.0 && r < 1.0);

        return r;
    }

    /**
     *
     */
    sint64 my_atoll(const char string[])
    {
        sint64 n;
        sscanf(string, S64_FORMAT, &n);

        return n;
    }

    /**
     *
     */
    int my_round(double x)
    {
        return int(floor(x+0.5));
    }

    /**
     *
     */
    void * my_malloc(int size)
    {
        void * address;

        ASSERT(size > 0);
        address = malloc(size);

        if (address == NULL)
        {
            my_fatal("my_malloc(): malloc(): %s\n", strerror(errno));
        }

        return address;
    }

    /**
     *
     */
    void * my_realloc(void * address, int size)
    {
        ASSERT(address != NULL);
        ASSERT(size > 0);

        address = realloc(address,size);

        if (address == NULL)
        {
            my_fatal("my_realloc(): realloc(): %s\n", strerror(errno));
        }

        return address;
    }

    /**
     *
     */
    void my_free(void * address)
    {
        ASSERT(address != NULL);
        free(address);
    }

    /**
     *
     */
    void my_log_open(const char file_name[])
    {
        ASSERT(file_name != NULL);
        LogFile = fopen(file_name, "a");

        if (LogFile != NULL)
        {
            /**
             * Buffer de linha.
             */
            setvbuf(LogFile, NULL, _IOLBF, 0);
        }
    }

    /**
     *
     */
    void my_log_close()
    {
        if (LogFile != NULL)
        {
            fclose(LogFile);
        }
    }

    /**
     *
     */
    void my_log(const char format[], ...)
    {
        va_list ap;
        ASSERT(format != NULL);

        if (LogFile != NULL)
        {
            va_start(ap, format);
            vfprintf(LogFile, format, ap);
            va_end(ap);
        }
    }

    /**
     *
     */
    void my_fatal(const char format[], ...)
    {
        va_list ap;
        ASSERT(format != NULL);

        va_start(ap, format);
        vfprintf(stderr, format, ap);

        if (LogFile != NULL)
        {
            vfprintf(LogFile, format, ap);
        }

        va_end(ap);

        if (Error)
        {
            /**
             * Falha recursivo.
             */
            my_log("POLYGLOT *** RECURSIVE ERROR ***\n");
            exit(EXIT_FAILURE);

            /**
             * Não é permitido fazer um aborto. PARE !
             * not call ~> abort() <~ method;
             */
        } else
        {
            Error = true;
            quit();
        }
    }

    /**
     *
     */
    bool my_file_read_line(FILE * file, char string[], int size)
    {
        int src, dst;
        int c;

        ASSERT(file != NULL);
        ASSERT(string != NULL);
        ASSERT(size > 0);

        if (fgets(string, size, file) == NULL)
        {
            if (feof(file))
            {
                return false;
            } else
            {
                /**
                 * Falha.
                 */
                my_fatal("my_file_read_line(): fgets(): %s\n", strerror(errno));
            }
        }

        /**
         * Remover CRs e LFs.
         */

        src = 0;
        dst = 0;

        while ((c=string[src++]) != '\0')
        {
            if (c != '\r' && c != '\n')
            {
                string[dst++] = c;
            }
        }

        string[dst] = '\0';

        return true;
    }

    /**
     *
     */
    bool my_string_empty(const char string[])
    {
        return string == NULL || string[0] == '\0';
    }

    /**
     *
     */
    bool my_string_equal(const char string_1[], const char string_2[])
    {
        ASSERT(string_1 != NULL);
        ASSERT(string_2 != NULL);

        return strcmp(string_1, string_2) == 0;
    }

    /**
     *
     */
    bool my_string_case_equal(const char string_1[], const char string_2[])
    {
        int c1, c2;

        ASSERT(string_1 != NULL);
        ASSERT(string_2 != NULL);

        while (true)
        {
            c1 = *string_1++;
            c2 = *string_2++;

            if (tolower(c1) != tolower(c2))
            {
                return false;
            }

            if (c1 == '\0')
            {
                return true;
            }
        }

        return false;
    }

    /**
     *
     */
    char * my_strdup(const char string[])
    {
        char * address;

        ASSERT(string != NULL);

        /**
         * strdup() não é ANSI C.
         */

        address = (char *) my_malloc(strlen(string)+1);
        strcpy(address, string);

        return address;
    }

    /**
     *
     */
    void my_string_clear(const char * * variable)
    {
        ASSERT(variable != NULL);

        if (*variable != NULL)
        {
            my_free((void*)(*variable));
            *variable = NULL;
        }
    }

    /**
     *
     */
    void my_string_set(const char * * variable, const char string[])
    {
        ASSERT(variable != NULL);
        ASSERT(string != NULL);

        if (*variable != NULL)
        {
            my_free((void*)(*variable));
        }

        *variable = my_strdup(string);
    }

    /**
     *
     */
    void my_timer_reset(my_timer_t * timer)
    {
        ASSERT(timer != NULL);

        timer->start_real = 0.0;
        timer->start_cpu = 0.0;
        timer->elapsed_real = 0.0;
        timer->elapsed_cpu = 0.0;
        timer->running = false;
    }

    /**
     *
     */
    void my_timer_start(my_timer_t * timer)
    {
        ASSERT(timer != NULL);
        ASSERT(timer->start_real == 0.0);
        ASSERT(timer->start_cpu == 0.0);
        ASSERT(!timer->running);

        timer->running = true;
        timer->start_real = now_real();
        timer->start_cpu = now_cpu();
    }

    /**
     *
     */
    void my_timer_stop(my_timer_t * timer)
    {
        ASSERT(timer != NULL);
        ASSERT(timer->running);

        timer->elapsed_real += now_real() - timer->start_real;
        timer->elapsed_cpu += now_cpu() - timer->start_cpu;
        timer->start_real = 0.0;
        timer->start_cpu = 0.0;
        timer->running = false;
    }

    /**
     *
     */
    double my_timer_elapsed_real(const my_timer_t * timer)
    {
        double elapsed;

        ASSERT(timer != NULL);
        elapsed = timer->elapsed_real;

        if (timer->running)
        {
            elapsed += now_real() - timer->start_real;
        }

        if (elapsed < 0.0)
        {
            elapsed = 0.0;
        }

        return elapsed;
    }

    /**
     *
     */
    double my_timer_elapsed_cpu(const my_timer_t * timer)
    {
        double elapsed;

        ASSERT(timer != NULL);
        elapsed = timer->elapsed_cpu;

        if (timer->running)
        {
            elapsed += now_cpu() - timer->start_cpu;
        }

        if (elapsed < 0.0)
        {
            elapsed = 0.0;
        }

        return elapsed;
    }

    /**
     *
     */
    double my_timer_cpu_usage(const my_timer_t * timer)
    {
        double real, cpu;
        double usage;

        ASSERT(timer != NULL);
        real = my_timer_elapsed_real(timer);
        cpu = my_timer_elapsed_cpu(timer);

        if (real <= 0.0 || cpu <= 0.0)
        {
            return 0.0;
        }

        usage = cpu / real;

        if (usage >= 1.0)
        {
            usage = 1.0;
        }

        return usage;
    }

    /**
     *
     */
    char const * compute_pkgdatadir()
    {
        char const *pkgdatadir = getenv("GNUCHESS_PKGDATADIR");

        return pkgdatadir ? pkgdatadir : PKGDATADIR;
    }
}
