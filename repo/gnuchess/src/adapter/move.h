/**
 * move.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef MOVE_H
#define MOVE_H

    #include "board.h"
    #include "util.h"


    /**
     * Submissa é a mulher do advogado com diploma de direito...
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         * a1a1 não pode ser um lance legal.
         */
        const int MoveNone = 0;

        /**
         *
         */
        const int MovePromoteKnight = 1 << 12;

        /**
         *
         */
        const int MovePromoteBishop = 2 << 12;

        /**
         *
         */
        const int MovePromoteRook = 3 << 12;

        /**
         *
         */
        const int MovePromoteQueen = 4 << 12;

        /**
         *
         */
        const int MoveFlags = 7 << 12;

        /**
         * Tipos.
         */

        /**
         *
         */
        typedef uint16 move_t;

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool move_is_ok(int move);

        /**
         *
         */
        extern int  move_make(int from, int to);

        /**
         *
         */
        extern int  move_make_flags(int from, int to, int flags);

        /**
         *
         */
        extern int  move_from(int move);

        /**
         *
         */
        extern int  move_to(int move);

        /**
         *
         */
        extern int  move_promote_hack(int move);

        /**
         *
         */
        extern bool move_is_capture(int move, const board_t * board);

        /**
         *
         */
        extern bool move_is_promote(int move);

        /**
         *
         */
        extern bool move_is_en_passant(int move, const board_t * board);

        /**
         *
         */
        extern bool move_is_castle(int move, const board_t * board);

        /**
         *
         */
        extern int  move_piece(int move, const board_t * board);

        /**
         *
         */
        extern int  move_capture(int move, const board_t * board);

        /**
         *
         */
        extern int  move_promote(int move, const board_t * board);

        /**
         *
         */
        extern bool move_is_check(int move, const board_t * board);

        /**
         *
         */
        extern bool move_is_mate(int move, const board_t * board);

        /**
         *
         */
        extern int  move_order(int move);

        /**
         *
         */
        extern bool move_to_can(int move, const board_t * board, char string[], int size);

        /**
         *
         */
        extern int  move_from_can(const char string[], const board_t * board);

        /**
         *
         */
        extern void move_disp(int move, const board_t * board);
    }

#endif
