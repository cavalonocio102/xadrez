/**
 * board.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef BOARD_H
#define BOARD_H

    #include "colour.h"
    #include "square.h"
    #include "util.h"

    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        const int Empty = 0;
        const int SideH = 0;
        const int SideA = 1;
        const int SideNb = 2;

        /**
         * Tipos.
         */

        /**
         *
         */
        struct board_t
        {
            /**
             *
             */
            uint8 square[SquareNb];

            /**
             *
             */
            sint8 pos[SquareNb];

            /**
             *
             */
            uint8 list[ColourNb][32];

            /**
             *
             */
            sint8 list_size[ColourNb];

            /**
             *
             */
            sint8 number[12];

            /**
             *
             */
            sint8 turn;

            /**
             *
             */
            uint8 castle[ColourNb][SideNb];

            /**
             *
             */
            uint8 ep_square;

            /**
             *
             */
            sint16 ply_nb;

            /**
             *
             */
            sint16 move_nb;

            /**
             *
             */
            uint64 key;
        };

        /**
         * Funções.
         */

        /**
         *
         */
        extern bool board_is_ok(const board_t * board);

        /**
         *
         */
        extern void board_clear(board_t * board);

        /**
         *
         */
        extern void board_start(board_t * board);

        /**
         *
         */
        extern void board_copy(board_t * dst, const board_t * src);

        /**
         *
         */
        extern bool board_equal(const board_t * board_1, const board_t * board_2);

        /**
         *
         */
        extern void board_init_list(board_t * board);

        /**
         *
         */
        extern int  board_flags(const board_t * board);

        /**
         *
         */
        extern bool board_can_play(const board_t * board);

        /**
         *
         */
        extern int  board_mobility(const board_t * board);

        /**
         *
         */
        extern bool board_is_check(const board_t * board);

        /**
         *
         */
        extern bool board_is_mate(const board_t * board);

        /**
         *
         */
        extern bool board_is_stalemate(const board_t * board);

        /**
         *
         */
        extern int  king_pos(const board_t * board, int colour);

        /**
         *
         */
        extern void board_disp(const board_t * board);
    }

#endif
