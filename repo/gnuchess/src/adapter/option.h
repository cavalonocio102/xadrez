/**
 * option.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef OPTION_H
#define OPTION_H

    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Funções.
         */

        /**
         *
         */
        extern void option_init();

        /**
         *
         */
        extern bool option_set(const char var[], const char val[]);

        /**
         *
         */
        extern const char * option_get(const char var[]);

        /**
         *
         */
        extern bool option_get_bool(const char var[]);

        /**
         *
         */
        extern double option_get_double(const char var[]);

        /**
         *
         */
        extern int option_get_int(const char var[]);

        /**
         *
         */
        extern const char * option_get_string (const char var[]);
    }

#endif
