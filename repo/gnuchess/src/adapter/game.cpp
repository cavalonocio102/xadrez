/**
 * game.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include "attack.h"
#include "board.h"
#include "fen.h"
#include "game.h"
#include "list.h"
#include "move.h"
#include "move_do.h"
#include "move_legal.h"
#include "piece.h"
#include "square.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Constantes.
     */
    static const bool UseSlowDebug = false;

    /**
     * Variáveis.
     */
    game_t Game[1];

    /**
     * Protótipos.
     */

    /**
     *
     */
    static void game_update(game_t * game);

    /**
     *
     */
    static int  game_comp_status(const game_t * game);

    /**
     * Funções.
     */

    /**
     *
     */
    bool game_is_ok(const game_t * game)
    {
        board_t board[1];
        int pos, move;

        if (game == NULL)
        {
            return false;
        }

        if (game->size < 0 || game->size > GameSize)
        {
            return false;
        }

        if (game->pos < 0 || game->pos > game->size)
        {
            return false;
        }

        /**
         * Modo DEBUG pesado opcional.
         */
        if (!UseSlowDebug)
        {
            return true;
        }

        if (!board_is_ok(game->start_board))
        {
            return false;
        }

        board_copy(board, game->start_board);


        for (pos = 0; pos <= game->size; pos++)
        {
            if (pos == game->pos)
            {
                if (!board_equal(game->board, board))
                {
                    return false;
                }
            }

            if (pos >= game->size)
            {
                break;
            }

            if (game->key[pos] != board->key)
            {
                return false;
            }

            move = game->move[pos];
            if (!move_is_legal(move, board))
            {
            }

            move_do(board, move);
        }

        if (game->status != game_comp_status(game))
        {
            return false;
        }

        return true;
    }

    /**
     *
     */
    void game_clear(game_t * game)
    {
        ASSERT(game != NULL);
        game_init(game, StartFen);
    }

    /**
     *
     */
    bool game_init(game_t * game, const char fen[])
    {
        ASSERT(game != NULL);
        ASSERT(fen != NULL);

        if (!board_from_fen(game->start_board, fen))
        {
            return false;
        }

        game->size = 0;
        board_copy(game->board, game->start_board);

        game->pos = 0;
        game_update(game);

        return true;
    }

    /**
     *
     */
    int game_status(const game_t * game)
    {
        ASSERT(game != NULL);

        return game->status;
    }

    /**
     *
     */
    int game_size(const game_t * game)
    {
        ASSERT(game != NULL);

        return game->size;
    }

    /**
     *
     */
    int game_pos(const game_t * game)
    {
        ASSERT(game != NULL);

        return game->pos;
    }

    /**
     *
     */
    int game_move(const game_t * game, int pos)
    {
        ASSERT(game != NULL);
        ASSERT(pos >= 0 && pos < game->pos);

        return game->move[pos];
    }

    /**
     *
     */
    void game_get_board(const game_t * game, board_t * board, int pos)
    {
        int start;
        int i;

        ASSERT(game != NULL);
        ASSERT(board != NULL);
        ASSERT(pos == -1 || (pos >= 0 && pos <= game->size));

        if (pos < 0)
        {
            pos = game->pos;
        }

        /**
         * A mão doi...
         */
        if (pos >= game->pos)
        {
            /**
             * Para a frente a partir da posição atual.
             */
            start = game->pos;
            board_copy(board,game->board);
        } else
        {
            /**
             * backward => repetir todo o jogo.
             */
            start = 0;
            board_copy(board, game->start_board);
        }

        for (i = start; i < pos; i++)
        {
            move_do(board, game->move[i]);
        }
    }

    /**
     *
     */
    int game_turn(const game_t * game)
    {
        ASSERT(game != NULL);

        return game->board->turn;
    }

    /**
     *
     */
    int game_move_nb(const game_t * game)
    {
        ASSERT(game != NULL);

        return game->board->move_nb;
    }

    /**
     *
     */
    void game_add_move(game_t * game, int move)
    {
        ASSERT(game != NULL);
        ASSERT(move_is_ok(move));
        ASSERT(move_is_legal(move, game->board));

        if (game->pos >= GameSize)
        {
            my_fatal("game_add_move(): game overflow\n");
        }

        game->move[game->pos] = move;
        game->key[game->pos] = game->board->key;

        move_do(game->board,move);
        game->pos++;

        /**
         * Jogo truncado.
         * Antes de chamar game_is_ok() em game_update().
         */
        game->size = game->pos;
        game_update(game);
    }

    /**
     *
     */
    void game_rem_move(game_t * game)
    {
        ASSERT(game != NULL);
        game_goto(game, game->pos - 1);

        /**
         * Jogo truncado.
         */
        game->size = game->pos;
    }

    /**
     *
     */
    void game_goto(game_t * game, int pos)
    {
        int i;

        ASSERT(game != NULL);
        ASSERT(pos >= 0 && pos <= game->size);

        if (pos < game->pos)
        {
            /**
             * Indo para trás => repetir todo o jogo.
             */
            board_copy(game->board, game->start_board);
            game->pos = 0;
        }

        for (i = game->pos; i < pos; i++)
        {
            move_do(game->board, game->move[i]);
        }

        ASSERT(i == pos);

        game->pos = pos;
        game_update(game);
    }

    /**
     *
     */
    void game_disp(const game_t * game)
    {
        board_t board[1];
        int i, move;

        ASSERT(game_is_ok(game));
        board_copy(board, game->start_board);
        board_disp(board);

        for (i = 0; i < game->pos; i++)
        {
            move = game->move[i];
            move_disp(move, board);
            move_do(board, move);
        }

        my_log("POLYGLOT\n");
        board_disp(board);
    }

    /**
     *
     */
    static void game_update(game_t * game)
    {
        ASSERT(game != NULL);
        game->status = game_comp_status(game);

        ASSERT(game_is_ok(game));
    }

    /**
     *
     */
    static int game_comp_status(const game_t * game)
    {
        int i, n;
        int wb, bb;
        const board_t * board;
        uint64 key;

        int start;
        ASSERT(game != NULL);

        /**
         * Iniciar.
         */
        board = game->board;

        /**
         * Companheiro e impasse.
         */

        if (!board_can_play(board))
        {
            if (false)
            {
            } else if (is_in_check(board, Black))
            {
                return WHITE_MATES;
            } else if (is_in_check(board, White))
            {
                return BLACK_MATES;
            } else
            {
                return STALEMATE;
            }
        }

        /**
         * Materiais insuficientes.
         */
        if (board->number[WhitePawn12]  == 0 &&
            board->number[BlackPawn12]  == 0 &&
            board->number[WhiteQueen12] == 0 &&
            board->number[BlackQueen12] == 0 &&
            board->number[WhiteRook12]  == 0 &&
            board->number[BlackRook12]  == 0)
        {
            if (board->number[WhiteBishop12] +
                board->number[BlackBishop12] +
                board->number[WhiteKnight12] +
                board->number[BlackKnight12] <= 1)
            {
                /**
                 *
                 */
                return DRAW_MATERIAL;
            } else if (board->number[WhiteBishop12] == 1 &&
                board->number[BlackBishop12] == 1 &&
                board->number[WhiteKnight12] == 0 &&
                board->number[BlackKnight12] == 0)
            {
                wb = board->list[White][1];
                bb = board->list[Black][1];

                if (square_colour(wb) == square_colour(bb))
                {
                    return DRAW_MATERIAL;
                }
            }
        }

        /**
         * Regra dos 50 movimentos.
         */
        if (board->ply_nb >= 100)
        {
            return DRAW_FIFTY;
        }

        /**
         * Repetição de posição.
         */
        key = board->key;
        n = 0;
        start = game->pos - board->ply_nb;

        if (start < 0)
        {
            start = 0;
        }

        for (i = game->pos - 4; i >= start; i -= 2)
        {
            if (game->key[i] == key)
            {
                if (++n == 2)
                {
                    return DRAW_REPETITION;
                }
            }
        }

        return PLAYING;
    }
}
