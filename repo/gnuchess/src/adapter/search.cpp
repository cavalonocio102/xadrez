/**
 * search.cpp - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "attack.h"
#include "board.h"
#include "colour.h"
#include "engine.h"
#include "fen.h"
#include "line.h"
#include "list.h"
#include "move.h"
#include "move_do.h"
#include "move_gen.h"
#include "move_legal.h"
#include "option.h"
#include "parse.h"
#include "san.h"
#include "search.h"
#include "uci.h"
#include "util.h"


/**
 *
 */
namespace adapter
{
    /**
     * Variáveis.
     */

    /**
     *
     */
    static int Depth;

    /**
     *
     */
    static int BestMove;

    /**
     *
     */
    static int BestValue;

    /**
     *
     */
    static move_t BestPV[LineSize];

    /**
     *
     */
    static sint64 NodeNb;

    /**
     *
     */
    static sint64 LeafNb;

    /**
     *
     */
    static double Time;

    /**
     *
     */
    static int Move;

    /**
     *
     */
    static int MovePos;

    /**
     *
     */
    static int MoveNb;

    /**
     * Protótipos.
     */

    /**
     *
     */
    static bool depth_is_ok(int depth);

    /**
     *
     */
    static void perft(const board_t * board, int depth);

    /**
     * Funções.
     */

    /**
     *
     */
    static bool depth_is_ok(int depth)
    {
        return depth >= 0 && depth < DepthMax;
    }

    /**
     *
     */
    void search(const board_t * board, int depth_max, double time_max)
    {
        char string[256];

        ASSERT(board_is_ok(board));
        ASSERT(depth_max >= 1 && depth_max < DepthMax);
        ASSERT(time_max >= 0.0);

        /**
         * Especificação.
         */

        Depth = 0;
        BestMove = MoveNone;
        BestValue = 0;
        line_clear(BestPV);

        NodeNb = 0;
        LeafNb = 0;
        Time = 0.0;
        Move = MoveNone;
        MovePos = 0;
        MoveNb = 0;

        /**
         * Iniciar.
         */
        uci_send_ucinewgame(Uci);
        uci_send_isready_sync(Uci);

        /**
         * Posição.
         */
        if (!board_to_fen(board, string, 256))
        {
            ASSERT(false);
        }

        engine_send(Engine, "position fen %s", string);

        /**
         * Iniciar nova busca.
         */
        engine_send_queue(Engine, "go");
        engine_send_queue(Engine, " movetime %.0f", time_max * 1000.0);
        engine_send_queue(Engine, " depth %d", depth_max);

        /**
         * Nova linha.
         */
        engine_send(Engine, "");

        /**
         * Central: Aguarde um feedback dessa missão...
         */
        while (true)
        {
            engine_get(Engine, string, 256);

            if (false)
            {
            } else if (match(string,"bestmove * ponder *"))
            {
                BestMove = move_from_can(Star[0], board);
                ASSERT(BestMove != MoveNone && move_is_legal(BestMove, board));
                break;
            } else if (match(string,"bestmove *"))
            {
                BestMove = move_from_can(Star[0], board);
                ASSERT(BestMove != MoveNone && move_is_legal(BestMove, board));

                break;
            }
        }

        printf("\n");
    }

    /**
     *
     */
    void search_perft(const board_t * board, int depth_max)
    {
        int depth;
        my_timer_t timer[1];

        double time, speed;
        ASSERT(board_is_ok(board));
        ASSERT(depth_max >= 1 && depth_max < DepthMax);

        /**
         * Iniciar.
         */
        board_disp(board);

        /**
         * Aprofundamento iterativo.
         */
        for (depth = 1; depth <= depth_max; depth++)
        {
            /**
             * Iniciar.
             */
            NodeNb = 0;
            LeafNb = 0;

            my_timer_reset(timer);
            my_timer_start(timer);
            perft(board, depth);
            my_timer_stop(timer);

            time = my_timer_elapsed_cpu(timer);
            speed = (time < 0.01) ? 0.0 : double(NodeNb) / time;

            printf("%2d %10lld %10lld %7.2f %7.0f\n", depth, NodeNb, LeafNb, time, speed);
        }

        printf("\n");
    }

    /**
     *
     */
    static void perft(const board_t * board, int depth)
    {
        int me;
        list_t list[1];

        int i, move;
        board_t new_board[1];

        ASSERT(board_is_ok(board));
        ASSERT(depth_is_ok(depth));
        ASSERT(!is_in_check(board, colour_opp(board->turn)));

        /**
         * Iniciar.
         */
        NodeNb++;

        /**
         * Folha: Apenas para quem ama um xadrez e quer sé entregar !
         */
        if (depth == 0)
        {
            LeafNb++;

            return;
        }

        /**
         * Mais inicialização.
         */
        me = board->turn;

        /**
         * Mover ciclo.
         */
        gen_moves(list, board);

        for (i = 0; i < list_size(list); i++)
        {
            move = list_move(list, i);
            board_copy(new_board, board);
            move_do(new_board, move);

            if (!is_in_check(new_board, me))
            {
                perft(new_board, depth - 1);
            }
        }
    }
}
