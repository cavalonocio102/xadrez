/**
 * move_gen.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef MOVE_GEN_H
#define MOVE_GEN_H

    #include "board.h"
    #include "list.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Funções.
         */

        /**
         *
         */
        extern void gen_legal_moves(list_t * list, const board_t * board);

        /**
         *
         */
        extern void gen_moves(list_t * list, const board_t * board);
    }

#endif
