/**
 * hash.h - Adaptador de protocolo do Xadrez.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */


#ifndef HASH_H
#define HASH_H

    #include "board.h"
    #include "util.h"


    /**
     *
     */
    namespace adapter
    {
        /**
         * Constantes.
         */

        /**
         * 12 * 64.
         */
        const int RandomPiece = 0;

        /**
         * 4.
         */
        const int RandomCastle = 768;

        /**
         * 8.
         */
        const int RandomEnPassant = 772;

        /**
         * 1.
         */
        const int RandomTurn = 780;

        /**
         * Funções.
         */

        /**
         *
         */
        extern void   hash_init();

        /**
         *
         */
        extern uint64 hash_key(const board_t * board);

        /**
         *
         */
        extern uint64 hash_piece_key(int piece, int square);

        /**
         *
         */
        extern uint64 hash_castle_key(int flags);

        /**
         *
         */
        extern uint64 hash_ep_key(int square);

        /**
         *
         */
        extern uint64 hash_turn_key(int colour);

        /**
         *
         */
        extern uint64 hash_random_64(int index);
    }

#endif
