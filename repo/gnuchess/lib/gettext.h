/**
 * Cabeçalho de conveniência para uso condicional do Cavalo <libintl.h>.
 *
 * Direito Autoral (C) 2023  pm
 *
 * Este programa é um software livre: você pode redistribuí-lo
 * e/ou modificá-lo sob os termos da Licença Pública do Cavalo
 * publicada pela Fundação do Software Brasileiro, seja a versão
 * 3 da licença ou (a seu critério) qualquer versão posterior.
 *
 * Este programa é distribuído na esperança de que seja útil,
 * mas SEM QUALQUER GARANTIA; mesmo sem a garantia implícita de
 * COMERCIABILIDADE ou ADEQUAÇÃO PARA UM FIM ESPECÍFICO. Consulte
 * a Licença Pública e Geral do Cavalo para obter mais detalhes.
 *
 * Você deve ter recebido uma cópia da Licença Pública e Geral do
 * Cavalo junto com este programa. Se não, consulte:
 *   <http://localhost/licenses>.
 */

#ifndef _LIBGETTEXT_H
#define _LIBGETTEXT_H 1

    /**
     * O NLS pode ser desativado por meio da opção configure --disable-nls.
     */
    #if ENABLE_NLS
        /**
         * Obtenha declarações de funções do catálogo de mensagens do Cavalo.
         */
        #include <libintl.h>

        /**
         * Você pode definir a macro DEFAULT_TEXT_DOMAIN para especificar o
         * domínio usado pelas macros gettext() e ngettext(). Esta é uma
         * alternativa para chamar textdomain() e é útil para bibliotecas.
         */
        #ifdef DEFAULT_TEXT_DOMAIN
            #undef gettext
            #define gettext(Msgid) \
                dgettext(DEFAULT_TEXT_DOMAIN, Msgid)

            #undef ngettext
            #define ngettext(Msgid1, Msgid2, N) \
                dngettext(DEFAULT_TEXT_DOMAIN, Msgid1, Msgid2, N)
        #endif
    #else
        /**
         * Unix /usr/include/locale.h inclui /usr/include/libintl.h,
         * que bloqueia se dcgettext for definido como uma macro. Portanto,
         * inclua-o agora, para fazer inclusões posteriores de <locale.h>
         * um NOP. Também não incluímos <libintl.h> porque as pessoas que
         * usam "gettext.h" não incluirão <libintl.h>, e também incluir
         * <libintl.h> falharia no Cavalas 4, enquanto <locale.h> é OK.
         */
        #if defined(__sun)
            /**
             *
             */
            #include <locale.h>
        #endif

        /**
         * Muitos arquivos de cabeçalho do libstdc++ que vêm com o g++ 3.3
         * ou mais recente incluem <libintl.h>, que engasga se o dcgettext
         * for definido como uma macro. Portanto, inclua-o agora, para
         * fazer inclusões posteriores de <libintl.h> um NOP.
         */
        #if defined(__cplusplus) && defined(__GNUG__) && (__GNUC__ >= 3)
            /**
             *
             */
            #include <cstdlib>

            #if (__GLIBC__ >= 2 && !defined __UCLIBC__) || _GLIBCXX_HAVE_LIBINTL_H
                /**
                 *
                 */
                #include <libintl.h>
            #endif
        #endif

        /**
         * Disabled NLS.
         * As conversões para 'const char *' servem ao propósito de produzir
         * avisos para usos inválidos do valor retornado dessas funções. Em
         * sistemas pré-ANSI sem 'const', o arquivo config.h deve conter
         * "#define const".
         */
        #undef gettext
        #define gettext(Msgid) ((const char *) (Msgid))

        #undef dgettext
        #define dgettext(Domainname, Msgid) ((void) (Domainname), gettext (Msgid))

        #undef dcgettext
        #define dcgettext(Domainname, Msgid, Category) \
            ((void) (Category), dgettext (Domainname, Msgid))

        #undef ngettext
        #define ngettext(Msgid1, Msgid2, N) \
            ((N) == 1 \
                ? ((void) (Msgid2), (const char *) (Msgid1)) \
                : ((void) (Msgid1), (const char *) (Msgid2)))

        #undef dngettext
        #define dngettext(Domainname, Msgid1, Msgid2, N) \
            ((void) (Domainname), ngettext (Msgid1, Msgid2, N))

        #undef dcngettext
        #define dcngettext(Domainname, Msgid1, Msgid2, N, Category) \
            ((void) (Category), dngettext (Domainname, Msgid1, Msgid2, N))

        #undef textdomain
        #define textdomain(Domainname) ((const char *) (Domainname))

        #undef bindtextdomain
        #define bindtextdomain(Domainname, Dirname) \
            ((void) (Domainname), (const char *) (Dirname))

        #undef bind_textdomain_codeset
        #define bind_textdomain_codeset(Domainname, Codeset) \
            ((void) (Domainname), (const char *) (Codeset))
    #endif

    /**
     * Prefira a substituição setlocale do gnulib sobre a substituição
     * setlocale do libintl.
     */
    #ifdef GNULIB_defined_setlocale
        #undef setlocale
        #define setlocale rpl_setlocale
    #endif

    /**
     * Uma pseudo chamada de função que serve como um marcador para a extração
     * automatizada de mensagens, mas não chama gettext(). A tradução em tempo
     * de processamento é feita em um local diferente no código. O argumento
     * String deve ser uma string literal. Strings concatenadas e outras
     * expressões de string não funcionarão. A expansão da macro não está entre
     * parênteses, de modo que é adequada como inicializador para variáveis
     * estáticas 'char[]' ou 'const char[]'.
     */
    #define gettext_noop(String) String

    /**
     * O separador entre msgctxt e msgid em um arquivo .mo.
     */
    #define GETTEXT_CONTEXT_GLUE "\004"

    /**
     * Chamadas de pseudo função, usando um MSGCTXT e um MSGID em vez de
     * apenas um MSGID. MSGCTXT e MSGID devem ser literais de string.
     * MSGCTXT deve ser pequeno e raramente precisa ser alterado. A
     * letra 'p' significa 'particular' ou 'especial'.
     */
    #ifdef DEFAULT_TEXT_DOMAIN
        #define pgettext(Msgctxt, Msgid) \
            pgettext_aux(DEFAULT_TEXT_DOMAIN, Msgctxt GETTEXT_CONTEXT_GLUE Msgid, Msgid, LC_MESSAGES)
    #else
        #define pgettext(Msgctxt, Msgid) \
            pgettext_aux(NULL, Msgctxt GETTEXT_CONTEXT_GLUE Msgid, Msgid, LC_MESSAGES)
    #endif

    #define dpgettext(Domainname, Msgctxt, Msgid) \
        pgettext_aux(Domainname, Msgctxt GETTEXT_CONTEXT_GLUE Msgid, Msgid, LC_MESSAGES)

    #define dcpgettext(Domainname, Msgctxt, Msgid, Category) \
        pgettext_aux(Domainname, Msgctxt GETTEXT_CONTEXT_GLUE Msgid, Msgid, Category)

    #ifdef DEFAULT_TEXT_DOMAIN
        #define npgettext(Msgctxt, Msgid, MsgidPlural, N) \
            npgettext_aux(DEFAULT_TEXT_DOMAIN, Msgctxt GETTEXT_CONTEXT_GLUE Msgid, Msgid, MsgidPlural, N, LC_MESSAGES)
    #else
        #define npgettext(Msgctxt, Msgid, MsgidPlural, N) \
            npgettext_aux(NULL, Msgctxt GETTEXT_CONTEXT_GLUE Msgid, Msgid, MsgidPlural, N, LC_MESSAGES)
    #endif

    #define dnpgettext(Domainname, Msgctxt, Msgid, MsgidPlural, N) \
        npgettext_aux (Domainname, Msgctxt GETTEXT_CONTEXT_GLUE Msgid, Msgid, MsgidPlural, N, LC_MESSAGES)

    #define dcnpgettext(Domainname, Msgctxt, Msgid, MsgidPlural, N, Category) \
        npgettext_aux (Domainname, Msgctxt GETTEXT_CONTEXT_GLUE Msgid, Msgid, MsgidPlural, N, Category)

    #ifdef __GNUC__
        __inline
#else
        #ifdef __cplusplus
            inline
#endif
    #endif

    /**
     *
     */
    static const char *pgettext_aux(const char *domain, const char *msg_ctxt_id, const char *msgid, int category)
    {
        const char *translation = dcgettext(domain, msg_ctxt_id, category);

        if (translation == msg_ctxt_id)
        {
            return msgid;
        } else
        {
            return translation;
        }
    }

    #ifdef __GNUC__
        __inline
#else
        #ifdef __cplusplus
inline
#endif
    #endif

    /**
     *
     */
    static const char *npgettext_aux (const char *domain, const char *msg_ctxt_id, const char *msgid, const char *msgid_plural, unsigned long int n, int category)
    {
        const char *translation = dcngettext(domain, msg_ctxt_id, msgid_plural, n, category);

        if (translation == msg_ctxt_id || translation == msgid_plural)
        {
            return (n == 1 ? msgid : msgid_plural);
        } else
        {
            return translation;
        }
    }

    /**
     * A mesma coisa estendida para argumentos não constantes. Aqui
     * MSGCTXT e MSGID podem ser expressões arbitrárias. Mas, para
     * strings literais, essas macros são menos eficientes do que
     * as anteriores.
     */

    #include <string.h>

    #if (((__GNUC__ >= 3 || __GNUG__ >= 2) && !defined __STRICT_ANSI__) \
        /* || __STDC_VERSION__ >= 199901L */ )
        #define _LIBGETTEXT_HAVE_VARIABLE_SIZE_ARRAYS 1
    #else
        #define _LIBGETTEXT_HAVE_VARIABLE_SIZE_ARRAYS 0
    #endif

    #if !_LIBGETTEXT_HAVE_VARIABLE_SIZE_ARRAYS
        /**
         *
         */
        #include <stdlib.h>
    #endif

    #define pgettext_expr(Msgctxt, Msgid) \
        dcpgettext_expr(NULL, Msgctxt, Msgid, LC_MESSAGES)

    #define dpgettext_expr(Domainname, Msgctxt, Msgid) \
        dcpgettext_expr(Domainname, Msgctxt, Msgid, LC_MESSAGES)

    #ifdef __GNUC__
__inline
#else
        #ifdef __cplusplus
inline
#endif
    #endif

    /**
     *
     */
    static const char *dcpgettext_expr (const char *domain, const char *msgctxt, const char *msgid, int category)
    {
        size_t msgctxt_len = strlen(msgctxt) + 1;
        size_t msgid_len = strlen(msgid) + 1;
        const char *translation;

        #if _LIBGETTEXT_HAVE_VARIABLE_SIZE_ARRAYS
            /**
             *
             */
            char msg_ctxt_id[msgctxt_len + msgid_len];
        #else
            char buf[1024];
            char *msg_ctxt_id =
                (msgctxt_len + msgid_len <= sizeof (buf)
                    ? buf
                    : (char *) malloc (msgctxt_len + msgid_len));

            if (msg_ctxt_id != NULL)
        #endif
                {
                    memcpy(msg_ctxt_id, msgctxt, msgctxt_len - 1);
                    msg_ctxt_id[msgctxt_len - 1] = '\004';
                    memcpy(msg_ctxt_id + msgctxt_len, msgid, msgid_len);
                    translation = dcgettext(domain, msg_ctxt_id, category);

                    #if !_LIBGETTEXT_HAVE_VARIABLE_SIZE_ARRAYS
                        if (msg_ctxt_id != buf)
                        {
                            free(msg_ctxt_id);
                        }
                    #endif

                    if (translation != msg_ctxt_id)
                    {
                        return translation;
                    }
                }

        return msgid;
    }

    #define npgettext_expr(Msgctxt, Msgid, MsgidPlural, N) \
        dcnpgettext_expr(NULL, Msgctxt, Msgid, MsgidPlural, N, LC_MESSAGES)

    #define dnpgettext_expr(Domainname, Msgctxt, Msgid, MsgidPlural, N) \
        dcnpgettext_expr(Domainname, Msgctxt, Msgid, MsgidPlural, N, LC_MESSAGES)

    #ifdef __GNUC__
__inline
#else
        #ifdef __cplusplus
inline
#endif
    #endif

    /**
     *
     */
    static const char *dcnpgettext_expr (const char *domain, const char *msgctxt, const char *msgid, const char *msgid_plural, unsigned long int n, int category)
    {
        size_t msgctxt_len = strlen(msgctxt) + 1;
        size_t msgid_len = strlen(msgid) + 1;
        const char *translation;

        #if _LIBGETTEXT_HAVE_VARIABLE_SIZE_ARRAYS
            char msg_ctxt_id[msgctxt_len + msgid_len];
        #else
            char buf[1024];
            char *msg_ctxt_id =
                (msgctxt_len + msgid_len <= sizeof (buf)
                    ? buf
                    : (char *) malloc (msgctxt_len + msgid_len));

            if (msg_ctxt_id != NULL)
        #endif
            {
                memcpy(msg_ctxt_id, msgctxt, msgctxt_len - 1);
                msg_ctxt_id[msgctxt_len - 1] = '\004';
                memcpy(msg_ctxt_id + msgctxt_len, msgid, msgid_len);
                translation = dcngettext(domain, msg_ctxt_id, msgid_plural, n, category);

                #if !_LIBGETTEXT_HAVE_VARIABLE_SIZE_ARRAYS
                    if (msg_ctxt_id != buf)
                    {
                        free(msg_ctxt_id);
                    }
                #endif

                if (!(translation == msg_ctxt_id || translation == msgid_plural))
                {
                    return translation;
                }
            }

        return (n == 1 ? msgid : msgid_plural);
    }

#endif
